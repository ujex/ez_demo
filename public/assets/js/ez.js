$(function () {
    
    $('.confirm').click(function(e) {
        e.preventDefault();
        if (window.confirm("Uwaga! Wprowadzane zmiany mogą być nieodwracalne! Czy jesteś pewien, że chcesz wykonać tą akcję?")) {
            location.href = this.href;
        }
    });
    
});
//Ez Plugin Copy to clipboard
$.fn.ezCopyToClipboard = function() {
    var aux = document.createElement("input");
    aux.setAttribute("value", this.attr('data-ez-content'));
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
};
//Ez Plugin Show Flash Message
$.fn.ezShowFlash = function(c) {
    if(c.message){
        this.find('span').text(c.message);
    }
    if(c.type){
        this.removeClass('alert-info').addClass('alert-' + c.type);
        this.find('i').removeClass('fa-info').addClass('fa-' + c.type);
    }
    this.delay(300).fadeIn('normal', function() {
        $(this).delay(1700).fadeOut();
    });
};