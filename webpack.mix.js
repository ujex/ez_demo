let mix = require('laravel-mix');




mix.js('resources/assets/js/main.js', 'public/js')
   .styles([
        'resources/assets/css/frontend.css',
    ], 'public/css/main.css')
   .styles([
        'resources/assets/css/login.css',
    ], 'public/css/login.css')
    .version();
