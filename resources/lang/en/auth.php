<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' => [
        'email' => 'E-Mail Address',
        'password' => 'Password',
        'remember' => 'Remember Me',
        'login' => 'Login',
        'login_fb' => 'Login with Facebook',
        'or' => 'OR',
        'forgot' => 'Forgot Password',
        'error' => 'Password or e-mail incorrect',
    ],
    'register' => [
        'email' => 'E-Mail Address',
        'password' => 'Password',
        'password_confirm' => 'Confirm Password',
        'name' => 'Name',
        'surname' => 'Surname',
        'register' => 'Register',
        'login_fb' => 'Register with Facebook',
        'or' => 'OR',
    ],
    'password' => [
        'email' => 'E-Mail Address',
        'password' => 'Password',
        'password_confirm' => 'Confirm Password',
        'reset' => 'Reset Password',
        'send_link' => 'Send Password Reset Link',
    ],
];
