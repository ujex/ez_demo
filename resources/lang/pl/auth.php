<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Błędny login lub hasło.',
    'throttle' => 'Za dużo nieudanych prób logowania. Proszę spróbować za :seconds sekund.',

    'login' => [
        'email' => 'Podaj adres e-mail',
        'password' => 'Podaj hasło',
        'remember' => 'Zapamiętaj mnie',
        'login' => 'Zaloguj',
        'login_fb' => 'Zaloguj się przez Facebook',
        'or' => 'LUB',
        'forgot' => 'Zapomniałem hasła',
        'error' => 'Nieprawidłowe hało lub e-mail',
    ],
    'register' => [
        'email' => 'Podaj adres e-mail',
        'password' => 'Podaj hasło',
        'password_confirm' => 'Potwierdź hasło',
        'name' => 'Imię',
        'surname' => 'Nazwisko',
        'register' => 'Rejestruj',
        'login_fb' => 'Zaloguj się przez Facebook',
        'or' => 'LUB',
    ],
    'password' => [
        'email' => 'Podaj adres e-mail',
        'password' => 'Podaj hasło',
        'password_confirm' => 'Potwierdź hasło',
        'reset' => 'Resetuj hasło',
        'send_link' => 'Wyślij',
    ],
];
