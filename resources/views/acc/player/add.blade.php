@extends('layouts.frontend')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
@endsection


@section('content')

<h2>Dodaj zawodnika Twojego konta</h2>

<div class="box">
  <div class="box-header">
    <h3 class="box-title">Search Player</h3>
  </div>
  {{ Form::open([
            'route' => ['l.a.player.add', 'loc' => $loc]
        ]) }}
  <div class="box-body">
    <div class="form-group form-group-lg">
      {{ Form::text('player_search', null, ['class' => 'form-control', 'id' => 'player_search']) }}
      {{ Form::hidden('player_id', null, ['id' => 'player_id']) }}
    </div>
  </div>  
  {{ Form::close() }}
</div>





@endsection


@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script>
$(function () {
    $('#player_search').focus();

    $('#player_search').autocomplete({
        source: "/api/player/search/ALL",
        minLength: 3,
        select: function (a, b) {
            var res = b.item;
            $('#player_id').val(res.id);
            console.log(res);
        }
    });
    $('#team_search').autocomplete({
        source: "/api/team/search",
        minLength: 3,
        select: function (a, b) {
            var res = b.item;
            $('#team_id_new').val(res.id);
        }
    });
});
</script>
@endsection