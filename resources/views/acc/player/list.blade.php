@extends('layouts.frontend')

@section('page-css')
@endsection


@section('content')

<h2>Lista zawodników przypisanych do Twojego konta</h2>
<div class="row">
  <div class="col-md-6"><p><a href="{{ route('l.a.player.add', ['loc' => $loc]) }}" class="btn btn-success"><i class="fa fa-plus"> </i> Dodaj kolejnego zawodnika do swojego konta</a></p></div>
  <div class="col-md-6"><p class="text-right"><a href="/panel/races" class="btn btn-success"><i class="fa fa-plus"> </i> Przejdź do wyboru wyścigów</a></p></div>
</div>

<div class="table-responsive">
  <table class="table table-hover">
    <tr>
      <th>Nazwisko</th>
      <th>Imię</th>
      <th>Data urodzenia</th>
      <th>Klub</th>
      <th>&nbsp;</th>
    </tr>
    @foreach($players as $player)
    <tr>
      <td>{{ $player->surname }}</td>
      <td>{{ $player->name }}</td>
      <td>{{ $player->birth_date }}</td>
      <td>{{ $player->team->name }}</td>
      <td>
        <a href="/panel/unmerge/3833" class="btn btn-danger confirm" data-toggle="tooltip" title="Usuń zawodnika ze swojego konta"><i class="fa fa-trash-o fa-lg"></i></a>
      </td>
    </tr>
    @endforeach
  </table>
</div>
@endsection


@section('page-script')
@endsection