@extends('layouts.doc')

@section('content')


<center><b style="font-size:18pt;">{{ $competition->name }}</b></center>
<center><b style="font-size:13pt;">{{ $competition->place }}</b></center>

<center><b style="font-size:11pt;">Zgłoszeni zawodnicy</b></center><br>

@if($competition->organiser)
<p style="font-size:9pt;"><b>Organizator: </b>{{ $competition->organiser }}</p>
@endif

<table cellpadding="1" cellspacing="1" border="0" style="font-size:8pt; font-family:{{ config('ez.print.doc_font') }};">
  <tr>
    <th style="text-align:left; width:25pt;">lp</th>
    <th style="text-align:left; width:70pt;">Nazwisko</th>
    <th style="text-align:left; width:70pt;">Imię</th>
    <th style="text-align:left; width:70pt;">Data ur.</th>
    <th style="text-align:left; width:70pt;">UCI ID</th>
    <th style="text-align:left; width:150pt;">Klub</th>
    <th style="text-align:left; width:65pt;">Telefon</th>
    <th style="text-align:left; width:75pt;">e-mail</th>
    <th style="text-align:left; width:75pt;">Data zgłoszenia</th>
  </tr>
  @foreach ($competition->players as $result)
  <tr style="{{ ez_odd($loop->iteration, 'background-color:#efefef') }}">
    <td>{{ $loop->iteration }}</td>
    <td>{{ $result->surname }}</td>
    <td>{{ $result->name }}</td>
    <td>{{ $result->birth_date }}</td>
    <td>{{ $result->uci_id }}</td>
    <td>{{ $result->team }}</td>
    <td>{{ $result->phone }}</td>
    <td>{{ $result->email }}</td>
    <td>{{ $result->created_at->format('d.m.Y') }}</td>
  </tr>
  @endforeach
</table>


@endsection