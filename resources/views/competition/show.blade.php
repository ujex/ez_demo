@extends('layouts.frontend')

@section('page-css')
@endsection


@section('content')

<h1 class="text-lowercase text-capitalize">{{ $competition->name }}</h1>
<div class="row">
    <div class="col-md-7">
      <span class="g">{{ $competition->date_start->format('d.m.Y') }} {{ $competition->date_end ? '- '.$competition->date_end->format('d.m.Y'):'' }} &bull; <a href="{{ route('l.competition.list', ['loc' => $loc, 'slug' => $competition->category->url]) }}">{{ $competition->category->name }}</a></span><br />
    </div>
    <div class="col-md-5 text-right">
        @if ($competition->registration_to >= date('Y-m-d'))
        <a href="{{ route('l.register.sign', ['code'=>$competition->code, 'loc'=>$loc]) }}" class="btn btn-info"><i class="fa fa-sign-in"></i> {{ __('site.comp.signin') }}</a>
        @endif    
        @if ($competition->rally->count())
        <a href="{{ route('l.competition.res', ['code'=>$competition->code, 'loc'=>$loc]) }}" class="btn btn-info"><i class="fa fa-sign-in"></i> {{ __('site.comp.res') }}</a>
        @endif  
    </div>
</div>
@if ($competition->reg->count())
<div>
  <h4 style="margin-top: 20px;">{{ __('site.comp.files') }}:</h4>
  <ul>
    @foreach ($competition->reg as $file)
    <li style="font-size:1.3em"><a 
          onClick="_gaq.push(['_trackEvent', 'Pobierz', '{{ $competition->name }}', '{{ $file->description }}']);" 
          href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}"><i class="fa fa-file-pdf-o fa-1x"></i> {{ $file->description }} 
      </a>
    </li>
    @endforeach
  </ul>
</div>
@endif
@if ($competition->res->count())
<div>
  <h4 style="margin-top: 20px;">{{ __('site.comp.reults') }}:</h4>
  <ul>
    @foreach ($competition->res as $file)
    <li style="font-size:1.3em"><a 
          onClick="_gaq.push(['_trackEvent', 'Pobierz', '{{ $competition->name }}', '{{ $file->description }}']);" 
          href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}"><i class="fa fa-file-pdf-o fa-1x"></i> {{ $file->description }} 
      </a>
    </li>
    @endforeach
  </ul>
</div>
@endif

@if ($competition->organiser)
<h4 style="margin-top: 20px;">{{ __('site.comp.organiser') }}:</h4><strong>{{ $competition->organiser }}</strong>        
@endif

@if ($competition->info)
<h4 style="margin-top: 20px;">{{ __('site.comp.info') }}:</h4><p>{!! $competition->info !!}</p>        
@endif

@if ($competition->lat && $competition->lng)
<div>
  <h4 style="margin-top: 20px;">{{ __('site.comp.location') }}: <a href="https://www.google.com/maps/dir/Current+Location/{{ $competition->lat }},{{ $competition->lng }}" target="_blank"><strong>{{ $competition->place }}</strong></a></h4>
  <div id="map"></div>
  <p>
    <a href="https://www.google.com/maps/dir/Current+Location/{{ $competition->lat }},{{ $competition->lng }}" target="_blank">{{ __('site.comp.nav') }}</a>
  </p>
  <script type="text/javascript">
      function initMap() {
      var lat = {{ $competition->lat }};
      var lng = {{ $competition->lng }};
      var myLatLng = {lat: lat, lng: lng};
      var map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
          scrollwheel: false,
          zoom: 14
      });
      var marker = new google.maps.Marker({
      map: map,
          position: myLatLng,
          title: '{{ __('site.comp.office') }}'
      });
      var contentString = '<div>' +
          '<h4>{{ __('site.comp.office') }}</h4>' +
          '</div>';
      var infowindow = new google.maps.InfoWindow({
      content: contentString
      });
      map.addListener('mouseover', function () {
      infowindow.open(map, marker);
      });
      }
  </script>
</div>
@endif
@endsection


@section('page-script')
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvtgtSMQFpof3VpVsFsPKknnVfgHXtO8I&callback=initMap">
</script>
@endsection