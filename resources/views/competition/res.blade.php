@extends('layouts.frontend')

@section('page-css')
@endsection


@section('content')

<h1 class="text-lowercase text-capitalize">{{ $competition->name }}</h1>
<div class="row">
    <div class="col-md-7">
      <span class="g">{{ $competition->date_start->format('d.m.Y') }} {{ $competition->date_end ? '- '.$competition->date_end->format('d.m.Y'):'' }} &bull; <a href="{{ route('l.competition.list', ['loc' => $loc, 'slug' => $competition->category->url]) }}">{{ $competition->category->name }}</a></span><br />
    </div>
    <div class="col-md-5 text-right">
        @if ($competition->registration_to >= date('Y-m-d'))
        <a href="{{ route('l.register.sign', ['code'=>$competition->code, 'loc'=>$loc]) }}" class="btn btn-info"><i class="fa fa-sign-in"></i> {{ __('site.comp.signin') }}</a>
        @endif            
    </div>
</div>
@if ($competition->reg->count())
<div>
  <h4 style="margin-top: 20px;">{{ __('site.comp.files') }}:</h4>
  <ul>
    @foreach ($competition->reg as $file)
    <li style="font-size:1.3em"><a 
          onClick="_gaq.push(['_trackEvent', 'Pobierz', '{{ $competition->name }}', '{{ $file->description }}']);" 
          href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}"><i class="fa fa-file-pdf-o fa-1x"></i> {{ $file->description }} 
      </a>
    </li>
    @endforeach
  </ul>
</div>
@endif
@if ($competition->res->count())
<div>
  <h4 style="margin-top: 20px;">{{ __('site.comp.reults') }}:</h4>
  <ul>
    @foreach ($competition->res as $file)
    <li style="font-size:1.3em"><a 
          onClick="_gaq.push(['_trackEvent', 'Pobierz', '{{ $competition->name }}', '{{ $file->description }}']);" 
          href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}"><i class="fa fa-file-pdf-o fa-1x"></i> {{ $file->description }} 
      </a>
    </li>
    @endforeach
  </ul>
</div>
@endif

@if ($competition->rally->count())
<h4 style="margin-top: 20px;">{{ __('site.comp.result') }}:</h4>
    <div class="container-fluid etab">
    <ul id="tabs" class="nav nav-tabs" >
        @foreach ($competition->rally as $rally)
            @if ($loop->iteration == 1)
            <li class="active"><a href="#c-{{ $rally->id }}" data-url="{{ route('ajax.result', ['id' => $rally->id]) }}">{{ $rally->name }}</a></li>
            @else
            <li><a href="#c-{{ $rally->id }}" data-url="{{ route('ajax.result', ['id' => $rally->id]) }}">{{ $rally->name }}</a></li>
            @endif
        @endforeach
    </ul>
    <div class="tab-content">
    @foreach ($competition->rally as $rally)
        @if ($loop->iteration == 1)
        <div class="tab-pane active" id="c-{{ $rally->id }}"></div>
        @else
        <div class="tab-pane" id="c-{{ $rally->id }}"></div>
        @endif
    @endforeach
    </div>
    </div>
@else
<div class="alert alert-warning">
    <p>{{ __('site.comp.no_result') }}</p>
</div>
@endif



@endsection


@section('page-script')
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvtgtSMQFpof3VpVsFsPKknnVfgHXtO8I&callback=initMap">
</script>
<script src="/js/bootstrap-tabdrop.js"></script>
    <script>
    $(function() {
        $('.nav-pills, .nav-tabs').tabdrop({text: '{{ __('site.comp.more') }}'});
        $('#tabs a').click(function (e) {
            e.preventDefault();
            var url = $(this).attr("data-url");
            var href = this.hash;
            var pane = $(this);
            $(href).load(url, function(result){      
                pane.tab('show');
            });
        });
        $('.tab-pane:first').load($('.active a').attr("data-url"), function(result){
            $('.active a').tab('show');
        });
    });
    </script>
@endsection