@if ($rally->length && $rally->speed)
<div class="row">
    <div class="col-md-6">{{ __('site.comp.ajax.distance') }}: <strong>{{$rally->length/1000}} km</strong></div>
    <div class="col-md-6 text-right">{{ __('site.comp.ajax.speed') }}: <strong>{{$rally->speed}} km/h</strong></div>
</div>
@endif
<div class="table-responsive">
<table class="table table-striped">
<tr>
    <th>{{ __('site.comp.ajax.th.place') }}</th>
    <th>{{ __('site.comp.ajax.th.surname') }} {{ __('site.comp.ajax.th.name') }}</th>
    <th class="text-right">{{ __('site.comp.ajax.th.result') }}</th>
    <th class="text-right">{{ __('site.comp.ajax.th.diff') }}</th>
</tr>
@if ($rally->result->count())
@foreach ($rally->result as $r)
<tr>
    <td><small>{{ $r->pos or '' }}</small></td>
    <td><span class="text-uppercase pn">{{ $r->surname }}</span> <span class="pn">{{ $r->name }}</span><br /><small>{{ $r->team }}</small></td>
    <td class="text-right"><small>{{ $r->result }}</small></td>
    <td class="text-right"><small>{{ $r->diff }}</small></td>
</tr>
@endforeach
@else
<tr>
    <td class="text-center" colspan="4"></td>
</tr>
@endif
</table>
</div>
@if ($rally->info)
<div class="row">
    <div class="col-md-12">{!! $rally->info !!}</div>
</div>
@endif