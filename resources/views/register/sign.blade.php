@extends('layouts.frontend')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/select2/select2.min.css') }}">
@endsection


@section('content')
<h1 class="text-lowercase text-capitalize">{{ __('site.reg.sign') }} {{ $competition->name }}</h1>
<div class="row">
  <div class="col-md-7">
    <span class="g">{{ $competition->date_start->format('d.m.Y') }} {{ $competition->date_end ? '- '.$competition->date_end->format('d.m.Y'):'' }} &bull; <a href="{{ route('l.competition.list', ['loc' => $loc, 'slug' => $competition->category->url]) }}">{{ $competition->category->name }}</a></span><br />
  </div>
</div>
@if ($competition->reg->count())
<div>
  <h4 style="margin-top: 20px;">{{ __('site.comp.files') }}:</h4>
  <ul>
    @foreach ($competition->reg as $file)
    <li style="font-size:1.3em"><a 
          onClick="_gaq.push(['_trackEvent', 'Pobierz', '{{ $competition->name }}', '{{ $file->description }}']);" 
          href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}"><i class="fa fa-file-pdf-o fa-1x"></i> {{ $file->description }} 
      </a>
    </li>
    @endforeach
  </ul>
</div>
@endif

<div class="row">
  <div class="col-md-12">

    <div class="box">
      <div class="box-header">
        <h3 class="box-title">{{ __('site.reg.new') }}</h3>
      </div>
      {{ Form::model($player, [
            'route' => ['l.register.sign', 'code'=>$competition->code, 'loc'=>$loc],
            'class' => 'form-horizontal'
        ]) }}
      <div class="box-body">
        @include('inc.errors')
        <div class="form-group form-group-lg">
          {{ Form::label('name', __('site.reg.name').'*', ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-7">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
        </div>

        <div class="form-group form-group-lg">
          {{ Form::label('surname', __('site.reg.surname').'*', ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-7">{{ Form::text('surname', null, ['class' => 'form-control']) }}</div>
        </div>

        <div class="form-group form-group-lg">
          {{ Form::label('birth_date', __('site.reg.birth').'*', ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-5">{{ Form::text('birth_date', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
          <div class="col-sm-2"><br>({{ __('site.reg.birth_desc') }})</div>
        </div>

        <div class="form-group form-group-lg">
          {{ Form::label('uci_id', __('site.reg.uciid').'', ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-7">{{ Form::text('uci_id', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
        </div>

        <div class="form-group form-group-lg">
          {{ Form::label('team', __('site.reg.team'), ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-7">{{ Form::text('team', null, ['class' => 'form-control']) }}</div>
        </div>

        <div class="form-group">
          {{ Form::label('email', __('site.reg.email').'*', ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-7">{{ Form::email('email', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group">
          {{ Form::label('phone', __('site.reg.phone').'*', ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-7">{{ Form::text('phone', null, ['class' => 'form-control']) }}</div>
        </div>
        
        @if ($competition->register_form)
            @include('register.custom.' . $competition->register_form)
        @else
            @include('register.custom.default')
        @endif

        <div class="box-footer text-center">
          {{ Form::submit(__('site.reg.save'), ['class' => 'btn btn-info']) }}
        </div>
        @if ($competition->info)
        <div class="form-group">
          <h4>{{ __('site.comp.info') }}</h4>
          {!! $competition->info !!}       
        </div>
        @endif


      </div>
      {{ Form::close() }}
    </div>    
  </div>
</div>

@if ($competition->players->count() && $competition->registered_list)
<div class="row">
  <hr>
  <div class="col-md-8 col-md-offset-2">
    <h4>{{ __('site.reg.added') }}:</h4>
    <ol>
    @foreach ($competition->players as $player)
    <li><span class="pn">{{ $player->name }} {{ $player->surname }}</span> - {{ $player->team or 'NIESTOWARZYSZONY' }}</li>
    @endforeach
    </ol>
  </div>
</div>
@endif


@endsection


@section('page-script')
<script src="{{ asset('assets/lte/plugins/select2/select2.full.min.js') }}"></script>
<script>
$(function () {

    $(".select2").select2();
});
</script>
@endsection