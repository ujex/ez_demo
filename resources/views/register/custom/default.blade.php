{{ Form::hidden('country_id', '144') }}
<div class="form-group">
  {{ Form::label('regulation', __('site.reg.regulation').'', ['class' => 'col-sm-3 control-label']) }}
  <div class="col-sm-8">{{ Form::checkbox('regulation', 1) }}
    <small>Oświadczam, że zapoznałem się z regulaminem wyścigu.</small>
  </div>
</div>