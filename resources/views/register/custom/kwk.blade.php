<div class="form-group">
  {{ Form::label('address', __('site.reg.address').'', ['class' => 'col-sm-3 control-label']) }}
  <div class="col-sm-8">{{ Form::textArea('address', null, ['class' => 'form-control']) }}</div>
</div>
<div class="form-group">
  {{ Form::label('country_id', trans('site.reg.country'), ['class' => 'col-sm-3 control-label']) }}
  <div class="col-sm-8">{{ Form::select('country_id', $country, null, ['class' => 'form-control select2']) }}</div>
</div>
<div class="form-group">
  {{ Form::label('regulation', __('site.reg.regulation').'', ['class' => 'col-sm-3 control-label']) }}
  <div class="col-sm-8">{{ Form::checkbox('regulation', 1) }}
    <small>Oświadczam, że zapoznałem się z regulaminem wyścigu i będę przestrzegał jego zapisów
oraz przepisów sportowych PZKol., a także respektował decyzje Komisji Sędziowskiej oraz
organizatora</small>
  </div>
</div>