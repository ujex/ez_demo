{{ Form::hidden('country_id', '144') }}
<div class="form-group">
  {{ Form::label('address', __('site.reg.address').'', ['class' => 'col-sm-3 control-label']) }}
  <div class="col-sm-8">{{ Form::textArea('address', null, ['class' => 'form-control']) }}</div>
</div>
<div class="form-group">
  {{ Form::label('regulation', __('site.reg.regulation').'', ['class' => 'col-sm-3 control-label']) }}
  <div class="col-sm-8">{{ Form::checkbox('regulation', 1) }}
    <small>Startuję na własną odpowiedzialność. Oświadczam, że zapoznałem się z treścią regulaminu Podhale Tour i zobowiązuję się do jego przestrzegania. Posiadam dobry stan zdrowia i nie ma przeciwwskazań medycznych i zdrowotnych do uczestnictwa w zawodach. W razie wypadku nie będę występował z roszczeniami odszkodowawczymi wobec organizatora, lub osób działających w jego imieniu oraz  wyrażam zgodę na udzielenie mi pierwszej pomocy.</small>
  </div>
</div>