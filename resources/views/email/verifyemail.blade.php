@component('mail::message')
# Verify e-mail

Click the Link To Verify Your Email

@component('mail::button', ['url' => url('/verifyemail/'.$email_token)])
Click the following button to verify your email
@endcomponent

Regards,<br>
{{ config('app.name') }}

If you’re having trouble clicking the "Verify Password" button, copy and paste the URL below into your web browser: 
{{ url('/verifyemail/' . $email_token) }}
@endcomponent