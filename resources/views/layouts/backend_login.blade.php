<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $title or '' }} Panel Administracyjny PZKol</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('assets/lte/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
  
  <link rel="stylesheet" href="{{ asset('assets/lte/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/lte/dist/css/skins/skin-yellow.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/ez.min.css') }}">
  @yield('page-css')
</head>
<body class="hold-transition login-page">
<div class="login-box">

  <!-- Content Wrapper. Contains page content -->
  <div class="login-box-body">
 
 @yield('content')
 
 
  </div>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script>is_scan=false;</script>
<!-- jQuery 2.2.0 -->
<script src="{{ asset('assets/lte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/lte/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/fastclick/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/lte/dist/js/app.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/jQuery-Scanner-Detection/jquery.scannerdetection.js') }}"></script>
<script src="{{ asset('assets/js/ez.min.js') }}"></script>

<!-- page script -->
@yield('page-script')
</body>
</html>
