<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $title or '' }} Panel Administracyjny PZKol</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('assets/lte/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
  
  <link rel="stylesheet" href="{{ asset('assets/lte/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/lte/dist/css/skins/skin-yellow.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/ez.min.css') }}">
  @yield('page-css')
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown messages-menu"><a href="/" title="Przejdź do serwisu" target="_blank">Przejdź do serwisu <i class="glyphicon glyphicon-new-window"></i></a></li>
        
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              @if (Auth::user()->sociales->count())
              <img src="{{ Auth::user()->sociales->first()->avatar }}" class="user-image" alt="{{ Auth::user()->name }}">
              @else
              <img src="{{ asset('assets/lte/dist/img/avatar5.png') }}" class="user-image" alt="{{ Auth::user()->name }}">
              @endif
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                @if (Auth::user()->sociales->count())
                <img src="{{ Auth::user()->sociales->first()->avatar }}" class="img-circle" alt="{{ Auth::user()->name }}">
                @else
                <img src="{{ asset('assets/lte/dist/img/avatar5.png') }}" class="img-circle" alt="{{ Auth::user()->name }}">
                @endif
                <p>
                  {{ Auth::user()->name }} {{ Auth::user()->surname }} - {{ Auth::user()->type }}
                  <small>Zarejestrowany od {{ Auth::user()->created_at->format('j.m.Y') }}</small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">

                </div>
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        @if (Entrust::can('p.home'))
        <li class="treeview {{ (Request::segment(2)=='dashboard' ? 'active' : '') }}">
          <a href="{{ route('p.home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        @endif
        
        @if (Entrust::can('p.calendar'))
        <li class="treeview {{ (Request::segment(2)=='calendar' ? 'active' : '') }}">
          <a href="{{ route('p.calendar') }}"><i class="fa fa-calendar"></i> <span>Kalendarz</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ route('p.calendar.list') }}"><i class="fa fa-list"></i> Lista wyścigów</a></li>
            <li><a href="{{ route('p.calendar') }}"><i class="fa fa-calendar-o"></i> Kalendarz</a></li>
            <li><a href="{{ route('p.calendar.add') }}"><i class="fa fa-plus"></i> Dodaj wyścig</a></li>
          </ul>
        </li>
        @endif
        
        @if (Entrust::can('p.page') || Entrust::can('p.page.commissarie') || Entrust::can('p.page.tor'))
        <li class="treeview {{ (Request::segment(2)=='page' ? 'active' : '') }}">
          <a href="{{ route('home') }}"><i class="fa fa-file-text"></i> <span>Strony statyczne</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ route('p.page.list') }}"><i class="fa fa-list"></i> Lista stron</a></li>
            @if (Entrust::can('p.page'))
            <li><a href="{{ route('p.page.add') }}"><i class="fa fa-plus"></i> Dodaj stronę</a></li>
            @endif
          </ul>
        </li>
        @endif
        
        @if (Entrust::can('p.note'))
        <li class="treeview {{ (Request::segment(2)=='note' ? 'active' : '') }}">
          <a href="{{ route('p.note.list') }}"><i class="fa fa-file-text-o"></i> <span>Opisy</span></a>
        </li>
        @endif
        
        @if (Entrust::can('p.file'))
        <li class="treeview {{ (Request::segment(2)=='file' ? 'active' : '') }}">
          <a href="{{ route('home') }}"><i class="fa fa-files-o"></i> <span>Manager plików</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ route('p.file.list') }}"><i class="fa fa-list"></i> Lista plików</a></li>
            <li><a href="{{ route('p.file.add') }}"><i class="fa fa-plus"></i> Dodaj plik</a></li>
          </ul>
        </li>
        @endif
        
        @if (Entrust::can(['p.user', 'p.user.role']))
        <li class="treeview {{ (Request::segment(2)=='user' ? 'active' : '') }}">
          <a href="{{ route('home') }}"><i class="fa fa-user"></i> <span>Użytkownicy</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ route('p.user.list') }}"><i class="fa fa-list"></i> Lista użytkowników</a></li>
            <li><a href="{{ route('p.user.add') }}"><i class="fa fa-plus"></i> Dodaj użytkownika</a></li>
            <li><a href="{{ route('p.user.role') }}"><i class="fa fa-users"></i> Lista Grup</a></li>
            <li><a href="{{ route('p.user.permission') }}"><i class="fa fa-tasks"></i> Lista Modułów</a></li>
          </ul>
        </li>
        @endif
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="alert alert-info alert-dismissible navbar-fixed-top" style="display:none;" id="flash">
      <h4><i class="icon fa fa-info"></i> <span>Link został skopiowany do schowka!</span></h4>
    </div>
 
 @yield('content')
 
 
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> {{config('ez.version') }}
    </div>
    <strong>Wykonano przez: <a href="http://e-zawody.pl">e-zawody.pl</a></strong> All rights reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script>is_scan=false;</script>
<!-- jQuery 2.2.0 -->
<script src="{{ asset('assets/lte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/lte/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/fastclick/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/lte/dist/js/app.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/jQuery-Scanner-Detection/jquery.scannerdetection.js') }}"></script>
<script src="{{ asset('assets/js/ez.min.js') }}"></script>

<!-- page script -->
@yield('page-script')
</body>
</html>
