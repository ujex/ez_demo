@extends('layouts.frontend')

@section('page-css')
@endsection


@section('content')

<h1 class="text-lowercase text-capitalize">{{ $page->title }}</h1>

<div>
{!! $page->content !!}
</div>

@endsection


@section('page-script')

@endsection