@extends('layouts.frontend')

@section('page-css')
<link href="{{ mix('/css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
@php
if(!isset($loc)) $loc = config('app.locale');
@endphp
<div>
  <div class="row">
    <div class="col-md-10 col-md-offset-2">
      <div class="panel panel-login">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <a href="{{ route('l.login', ['loc' => $loc]) }}" id="login-form-link">{{ __('site.menu.login') }}</a>
            </div>
            <div class="col-xs-6">
              <a href="{{ route('l.register', ['loc' => $loc]) }}" class="active" id="register-form-link">{{ __('site.menu.register') }}</a>
            </div>
          </div>
          <hr>
        </div>

        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name" class="col-md-4 control-label">{{ __('auth.register.name') }}</label>

              <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
              <label for="surname" class="col-md-4 control-label">{{ __('auth.register.surname') }}</label>

              <div class="col-md-6">
                <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" required autofocus>

                @if ($errors->has('surname'))
                <span class="help-block">
                  <strong>{{ $errors->first('surname') }}</strong>
                </span>
                @endif
              </div>
            </div>
            
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="col-md-4 control-label">{{ __('auth.register.email') }}</label>

              <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="col-md-4 control-label">{{ __('auth.register.password') }}</label>

              <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <label for="password-confirm" class="col-md-4 control-label">{{ __('auth.register.password_confirm') }}</label>

              <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-8 col-md-offset-3">
                <button type="submit" class="btn btn-primary">{{ __('auth.register.register') }}</button>
                {{ __('auth.register.or') }}  
                <a href="{{url('/redirect')}}" class="btn btn-primary"><i class="fa fa-facebook"></i> {{ __('auth.register.login_fb') }}</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
