@extends('layouts.frontend')

@section('page-css')
<link href="{{ mix('/css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
@php
if(!isset($loc)) $loc = config('app.locale');
@endphp
<div>
  <div class="row">
    <div class="col-md-10 col-md-offset-2">
      <div class="panel panel-login">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <a href="{{ route('l.login', ['loc' => $loc]) }}" class="active" id="login-form-link">{{ __('site.menu.login') }}</a>
            </div>
            <div class="col-xs-6">
              <a href="{{ route('l.register', ['loc' => $loc]) }}" id="register-form-link">{{ __('site.menu.register') }}</a>
            </div>
          </div>
          <hr>
        </div>

        <div class="panel-body">
          
      @if ($errors->has('email') || $errors->has('password'))
      <div class="alert alert-danger">{{ __('auth.login.error') }}</div>
      @endif
          
          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="col-md-4 control-label">{{ __('auth.login.email') }}</label>

              <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
              </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="col-md-4 control-label">{{ __('auth.login.password') }}</label>

              <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('auth.login.remember') }}
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary">{{ __('auth.login.login') }}</button>
                {{ __('auth.login.or') }} 
                <a href="{{url('/redirect')}}" class="btn btn-primary"><i class="fa fa-facebook"></i> {{ __('auth.login.login_fb') }}</a>
                <br>
                <a class="btn btn-link" href="{{ route('password.request') }}">{{ __('auth.login.forgot') }}</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
