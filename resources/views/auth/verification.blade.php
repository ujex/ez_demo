@extends('layouts.frontend')

@section('page-css')
<link href="{{ mix('/css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
@php
if(!isset($loc)) $loc = config('app.locale');
@endphp
<div>
  <div class="row">
    <div class="col-md-10 col-md-offset-2">
      
      
<div class="panel panel-default">
<div class="panel-heading">Registration</div>
<div class="panel-body">
You have successfully registered. An email is sent to you for verification.
</div>
</div>
      
      
      
    </div>
  </div>
</div>
@endsection
