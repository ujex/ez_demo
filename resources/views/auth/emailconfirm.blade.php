@extends('layouts.frontend')

@section('content')

<div>
  <div class="row">
    <div class="col-md-10 col-md-offset-2">
      
      
      
      
      

<div class="panel panel-default">
<div class="panel-heading">Registration Confirmed</div>
<div class="panel-body">
Your Email is successfully verified.
</div>
</div>
     
      
      
    </div>
  </div>
</div>
@endsection
