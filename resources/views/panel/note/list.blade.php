@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
          <div class="box-tools pull-right">
            {{ Form::open(['route' => ['p.note.list'], 'method' => 'get']) }}
            <div class="has-feedback">
              {{ Form::text('q', $q, ['class' => 'form-control input-sm', 'placeholder' => 'Szukaj...', 'autocomplete' => 'off']) }}
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            {{ Form::close() }}
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Nazwa</th>
                <th>Treść</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if ($notes)
              @foreach ($notes as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                  <a href="{{ route('p.note.edit', ['id' => $row->id]) }}">
                    {{ $row->code }}
                    @if ($row->title)
                    <br>{{ $row->title }}
                    @endif
                  </a>
                </td>
                <td>{{ str_limit($row->content, 70) }}</td>
                <td>{!! ez_status($row->status) !!}</td>
                <td><a href="{{ route('p.note.edit', ['id' => $row->id]) }}" class="btn btn-block btn-primary btn-xs">Edytuj <i class="fa fa-edit"></></a></td>
              </tr>
              @endforeach
              @endif


            </tbody>
          </table>
          {{ $q ? $notes->appends(['q' => $q])->links() : $notes->links()  }}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
