@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
        </div>
        {{ Form::model($note, [
            'route' => ['p.note.edit', 'id' => $note->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) }}
        <div class="box-body">
          @include('panel.inc.errors')
          <div class="form-group">
            {{ Form::label('code', 'Kod', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::text('code', null, ['class' => 'form-control', 'readonly' => true]) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('title', 'Tytuł', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::text('title', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('content', 'Treść', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::textArea('content', null, ['class' => 'form-control']) }}</div>
          </div>


          <div class="box-footer">
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
        <div class="box-footer">

        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')

@endsection