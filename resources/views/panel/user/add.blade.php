@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    @include('panel.inc.errors')
  </div>
  <div class="row">
    <div class="col-md-7">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Dane użytkownika</h3>
        </div>
        {{ Form::model($user, [
            'route' => ['p.user.add'],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          <div class="form-group">
            {{ Form::label('firstname', 'Imię', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('firstname', $user->name, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('surname', 'Nazwisko', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('surname', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('email', 'e-mail', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('email', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('status', 'Status', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('status', [
                1 => 'Aktywny',
                0 => 'Nieaktywny',
              ], null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('pass', 'Hasło', ['class' => 'col-sm-2 control-label pass']) }}
            <div class="col-sm-10">{{ Form::text('pass', null, ['class' => 'form-control pass', 'autocomplete' => 'off']) }}</div>
          </div>
          <div id="pswd_info">
            <h4>Hasło musi spełniać poniższe wymagania:</h4>
            <ul class="list-unstyled">
              <li id="letter" class="invalid">Przynajmnijej <strong>jedna litera</strong></li>
              <li id="capital" class="invalid">Przynajmnijej <strong>jedna duża litera</strong></li>
              <li id="number" class="invalid">Przynajmnijej <strong>jedna cyfra</strong></li>
              <li id="length" class="invalid">Przynajmniej <strong>8 znaków</strong></li>
            </ul>
          </div>

          <br><br><br>
          <div class="box-footer">
            {{ Form::hidden('is_data_form', 1) }}
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>

</section>
@endsection
@section('page-script')

<script>
    $(function () {
        $('.pass').keyup(function () {
            var pswd = $(this).val();
            var length = false,
            letter = false,
            capital = false,
            number = false;
            //validate the length
            if (pswd.length < 8) {
                $('#length').removeClass('valid').addClass('invalid');
                length = false;
            } else {
                $('#length').removeClass('invalid').addClass('valid');
                length = true;
            }
            //validate letter
            if (pswd.match(/[A-z]/)) {
                $('#letter').removeClass('invalid').addClass('valid');
                letter = true;
            } else {
                $('#letter').removeClass('valid').addClass('invalid');
                letter = false;
            }
            //validate capital letter
            if (pswd.match(/[A-Z]/)) {
                $('#capital').removeClass('invalid').addClass('valid');
                capital = true;
            } else {
                $('#capital').removeClass('valid').addClass('invalid');
                capital = false;
            }
            //validate number
            if (pswd.match(/\d/)) {
                $('#number').removeClass('invalid').addClass('valid');
                number = true;
            } else {
                $('#number').removeClass('valid').addClass('invalid');
                number = false;
            }
            if (length && letter && capital && number) {
                $('#pass_btn').attr("disabled", false);
            } else {
                $('#pass_btn').attr("disabled", true); //true
            }
        }).focus(function () {
            $('#pswd_info').show();
        }).blur(function () {
            $('#pswd_info').hide();
        });
    });
</script>
@endsection