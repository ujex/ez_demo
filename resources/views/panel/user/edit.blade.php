@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
      @include('panel.inc.errors')
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Dane użytkownika</h3>
        </div>
        {{ Form::model($user, [
            'route' => ['p.user.edit', 'id' => $user->id],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          <div class="form-group">
            {{ Form::label('firstname', 'Imię', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('firstname', $user->name, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('surname', 'Nazwisko', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('surname', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('email', 'e-mail', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('email', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('status', 'Status', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('status', [
                0 => 'Nieaktywny',
                1 => 'Aktywny',
              ], null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="box-footer">
            {{ Form::hidden('is_data_form', 1) }}
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">Dane logowania</h3>
        </div>
        {{ Form::model($user, [
            'route' => ['p.user.edit', 'id' => $user->id],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          <div class="alert alert-warning">
            Uwaga!<br>
            Po zmianie hasła użytkownik otrzyma wiadomność e-mail z nowym hasłem!
          </div>
          <div class="form-group">
            {{ Form::label('login', 'Login', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('login', null, ['class' => 'form-control', 'disabled' => true]) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('pass', 'Hasło', ['class' => 'col-sm-2 control-label pass']) }}
            <div class="col-sm-10">{{ Form::text('pass', null, ['class' => 'form-control pass', 'autocomplete' => 'off']) }}</div>
          </div>
          <div id="pswd_info">
            <h4>Hasło musi spełniać poniższe wymagania:</h4>
            <ul class="list-unstyled">
              <li id="letter" class="invalid">Przynajmnijej <strong>jedna litera</strong></li>
              <li id="capital" class="invalid">Przynajmnijej <strong>jedna duża litera</strong></li>
              <li id="number" class="invalid">Przynajmnijej <strong>jedna cyfra</strong></li>
              <li id="length" class="invalid">Przynajmniej <strong>8 znaków</strong></li>
            </ul>
          </div>

          <br><br><br>
          <div class="box-footer">
            {{ Form::hidden('is_auth_form', 1) }}
            {{ Form::submit('Zapisz', ['class' => 'btn btn-danger pull-right', 'id' => 'pass_btn', 'disabled' => true]) }}
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Grupy przypisane</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Id</th>
                <th>Kod</th>
                <th>Nazwa</th>
                <th>Data utworzenia</th>
                <th>#</th>
              </tr>
            </thead>
            <tbody>
              @if(count($role_set))
              @foreach ($role_set as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $row['id'] }}</td>
                <td>{{ $row['name'] }}</td>
                <td>{{ $row['display_name'] }}</td>
                <td>{{ $row['created_at'] }}</td>
                <td><a href="{{ route('p.user.edit', ['id' => $user->id, 'role' => $row['id'], 'option' => 0]) }}" class="text-red"><i class="fa fa-minus-circle"></i></a></td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">Grupy nieprzypisane</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Id</th>
                <th>Kod</th>
                <th>Nazwa</th>
                <th>Data utworzenia</th>
                <th>#</th>
              </tr>
            </thead>
            <tbody>
              @if(count($role_not_set))
              @foreach ($role_not_set as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $row['id'] }}</td>
                <td>{{ $row['name'] }}</td>
                <td>{{ $row['display_name'] }}</td>
                <td>{{ $row['created_at'] }}</td>
                <td><a href="{{ route('p.user.edit', ['id' => $user->id, 'role' => $row['id'], 'option' => 1]) }}" class="text-green"><i class="fa fa-plus-circle"></i></a></td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>    
  </div>
</section>
@endsection
@section('page-script')

<script>
    $(function () {
        $('.pass').keyup(function () {
            var pswd = $(this).val();
            var length = false,
                letter = false,
                capital = false,
                number = false;
            //validate the length
            if (pswd.length < 8) {
                $('#length').removeClass('valid').addClass('invalid');
                length = false;
            } else {
                $('#length').removeClass('invalid').addClass('valid');
                length = true;
            }
            //validate letter
            if ( pswd.match(/[A-z]/) ) {
                $('#letter').removeClass('invalid').addClass('valid');
                letter = true;
            } else {
                $('#letter').removeClass('valid').addClass('invalid');
                letter = false;
            }
            //validate capital letter
            if ( pswd.match(/[A-Z]/) ) {
                $('#capital').removeClass('invalid').addClass('valid');
                capital = true;
            } else {
                $('#capital').removeClass('valid').addClass('invalid');
                capital = false;
            }
            //validate number
            if ( pswd.match(/\d/) ) {
                $('#number').removeClass('invalid').addClass('valid');
                number = true;
            } else {
                $('#number').removeClass('valid').addClass('invalid');
                number = false;
            }
            if(length && letter && capital && number){
                $('#pass_btn').attr("disabled", false);
            }else{
                $('#pass_btn').attr("disabled", true); //true
            }
        }).focus(function () {
            $('#pswd_info').show();
        }).blur(function () {
            $('#pswd_info').hide();
        });
    });
</script>
@endsection