@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
          <div class="box-tools pull-right">
              <button data-toggle="modal" data-target="#modalAddPermission" type="button" class="btn btn-default btn-block btn-flat bg-olive">Dodaj moduł</button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Kod</th>
              <th>Nazwa</th>
              <th>Opis</th>
              <th>Data utworzenia</th>
            </tr>
            </thead>
            <tbody>
        @foreach ($permissions as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $row->id }}</td>
              <td>{{ $row->name }}</td>
              <td>{{ $row->display_name }}</td>
              <td>{{ $row->description }}</td>
              <td>{{ $row->created_at }}</td>
            </tr>
        @endforeach


            </tbody>
          </table>
            {{ $permissions->links() }}
        </div>
      </div>
    </div>
  </div>
</section>


<div class="modal" id="modalAddPermission">
  <div class="modal-dialog">
    <div class="modal-content">
    {{ Form::model($perm_model, [
        'route' => ['p.user.permission.add'],
        'class' => 'form-horizontal'
    ]) }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Anuluj">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dodaj moduł</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            {{ Form::label('name', 'Kod', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('display_name', 'Nazwa', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('display_name', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('description', 'Opis', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('description', null, ['class' => 'form-control']) }}</div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Anuluj</button>
        <input type="submit" name="submit" class="btn btn-primary" value="Zapisz" />
      </div>
    {{ Form::close() }}
    </div>
  </div>
</div>
@endsection
