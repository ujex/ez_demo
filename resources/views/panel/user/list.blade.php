@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
          <div class="box-tools pull-right">
            {{ Form::open(['route' => ['p.user.list'], 'method' => 'get']) }}
            <div class="has-feedback">
              {{ Form::text('q', null, ['class' => 'form-control input-sm', 'placeholder' => 'Szukaj...']) }}
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            {{ Form::close() }}
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Nazwisko Imię</th>
              <th>e-mail</th>
              <th>Provider</th>
              <th>Avatar</th>
              <th>Typ</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
        @foreach ($users as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $row->id }}</td>
              <td>
                  <a href="{{ route('p.user.edit', ['id' => $row->id]) }}">
                      <strong>{{ $row->surname }}, {{ $row->name }}</strong>
                  </a>
              </td>
              <td>{{ $row->email }}</td>
              @if ($row->sociales->count())
              <td><a href="{{ $row->sociales->first()->profile_url }}" target="_blank">{{ $row->sociales->first()->provider }}</a></td>
              <td><img style="height:21px;" src="{{ $row->sociales->first()->avatar }}"></td>
              @else
              <td></td>
              <td></td>
              @endif
              <td>{{ $row->type }}</td>
              <td>{!! ez_status($row->status) !!}</td>
            </tr>
        @endforeach


            </tbody>
          </table>
            {{ $users->links() }}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
