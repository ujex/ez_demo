@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Przypisane</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Kod</th>
              <th>Nazwa</th>
              <th>Data utworzenia</th>
              <th>#</th>
            </tr>
            </thead>
            <tbody>
        @if(count($perm_set))
        @foreach ($perm_set as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $row['id'] }}</td>
              <td>{{ $row['name'] }}</td>
              <td>{{ $row['display_name'] }}</td>
              <td>{{ $row['created_at'] }}</td>
              <td><a href="{{ route('p.user.role.edit', ['id' => $role->id, 'perm' => $row['id'], 'option' => 0]) }}" class="text-red"><i class="fa fa-minus-circle"></i></a></td>
            </tr>
        @endforeach
        @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
      
    <div class="col-md-6">
      <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">Nieprzypisane</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Kod</th>
              <th>Nazwa</th>
              <th>Data utworzenia</th>
              <th>#</th>
            </tr>
            </thead>
            <tbody>
        @if(count($perm_not_set))
        @foreach ($perm_not_set as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $row['id'] }}</td>
              <td>{{ $row['name'] }}</td>
              <td>{{ $row['display_name'] }}</td>
              <td>{{ $row['created_at'] }}</td>
              <td><a href="{{ route('p.user.role.edit', ['id' => $role->id, 'perm' => $row['id'], 'option' => 1]) }}" class="text-green"><i class="fa fa-plus-circle"></i></a></td>
            </tr>
        @endforeach
        @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
