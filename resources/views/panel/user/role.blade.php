@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Kod</th>
              <th>Nazwa</th>
              <th>Opis</th>
              <th>Data utworzenia</th>
            </tr>
            </thead>
            <tbody>
        @foreach ($roles as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $row->id }}</td>
              <td><a href="{{ route('p.user.role.edit', ['id' => $row->id]) }}">{{ $row->name }}</a></td>
              <td>{{ $row->display_name }}</td>
              <td>{{ $row->description }}</td>
              <td>{{ $row->created_at }}</td>
            </tr>
        @endforeach


            </tbody>
          </table>
            {{ $roles->links() }}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
