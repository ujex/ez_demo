@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-9">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
        </div>
        {{ Form::model($page, [
            'route' => ['p.page.add'],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          @include('panel.inc.errors')
          <div class="form-group">
            {{ Form::label('title', 'Tytuł*', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::text('title', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('content', 'Treść*', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::textArea('content', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('lang', 'Language', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::select('lang', [
                'en' => 'EN',
                'pl' => 'PL',
              ], null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('status', 'Status', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::select('status', [
                1 => 'Aktywny',
                0 => 'Nieaktywny',
              ], 0, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('permission', 'Uprawnienia', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::text('permission', null, ['class' => 'form-control']) }}</div>
          </div>


          <div class="box-footer">
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
        <div class="box-footer">
          @include('panel.inc.form_required', ['form_required'=>true])
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
    @include('panel.inc.tinymce')
@endsection