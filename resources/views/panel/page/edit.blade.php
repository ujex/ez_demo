@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-9">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
        </div>
        {{ Form::model($page, [
            'route' => ['p.page.edit', 'id' => $page->id, 'redirect' => $redirect],
            'class' => 'form-horizontal',
            'files' => true
        ]) }}
        <div class="box-body">
          @include('panel.inc.errors')
          <div class="form-group">
            {{ Form::label('title', 'Tytuł*', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::text('title', null, ['class' => 'form-control']+$disabled['title']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('content', 'Treść*', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::textArea('content', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('lang', 'Language', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::select('lang', [
                'en' => 'EN',
                'pl' => 'PL',
              ], null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('status', 'Status', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::select('status', [
                0 => 'Nieaktywny',
                1 => 'Aktywny',
              ], null, ['class' => 'form-control']) }}</div>
          </div>
          @if (Entrust::can('p.page'))
          <div class="form-group">
            {{ Form::label('permission', 'Uprawnienia', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::text('permission', null, ['class' => 'form-control']) }}</div>
          </div>
          @endif
          <div class="form-group" >
            {{ Form::label('img_file', 'Zdjęcie', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::file('img_file', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="box-footer">
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
        <div class="box-footer">
          @include('panel.inc.form_required', ['form_required'=>true])
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-body box-profile">
          <h4><small>Dodany przez:</small> {{ $page_info->user->getName() }}</h4>
          <h5><small>Data dodania:</small> {{ $page_info->created_at }}</h5>
          <h5><small>Data edycji:</small> {{ $page_info->updated_at }}</h5>
          <a href="{{ route('l.page.show', ['id' => $page_info->id, 'slug' => $page_info->url, 'loc' => 'pl']) }}" target="_blank" class="text-red">Zobacz w serwisie <i class="glyphicon glyphicon-new-window"></i></a>
        </div>
      </div>
      @if ($page_info->img)
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Zdjęcie do artykułu</h3>
        </div>
        <div class="box-body">
          <img src="{{ asset('img/thumbnail/300/' . $page_info->img->name) }}" class="img-thumbnail text-center" width="100%">
          <div class="mailbox-attachment-info">
            <a href="{{ route('p.page.edit', ['id' => $page_info->id, 'unlink' => true, 'image_id' => $page_info->img->id]) }}" class="mailbox-attachment-name text-red confirm" data-toggle="tooltip" title="Usuń zdjęcie"><i class="fa fa-remove"></i></a> {{ $page_info->img->description or '' }}
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</section>
@endsection

@section('page-script')
@include('panel.inc.tinymce')
@endsection