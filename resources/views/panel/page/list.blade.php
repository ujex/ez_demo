@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
          <div class="box-tools pull-right">
            {{ Form::open(['route' => ['p.page.list'], 'method' => 'get']) }}
            <div class="has-feedback">
              {{ Form::text('q', $q, ['class' => 'form-control input-sm', 'placeholder' => 'Szukaj...', 'autocomplete' => 'off']) }}
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            {{ Form::close() }}
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Tytuł</th>
                <th>Element menu</th>
                <th>Status</th>
                <th>Data aktualizacji</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if ($pages)
              @foreach ($pages as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                  <a href="{{ route('p.page.edit', ['id' => $row->id]) }}">
                    {!! ez_flag($row->lang) !!}
                    <strong>{{ $row->title }}</strong>
                  </a>
                  <a href="{{ route('l.page.show', ['id' => $row->id, 'slug' => $row->url, 'loc' => 'pl']) }}" target="_blank" class="text-red"> <i class="glyphicon glyphicon-new-window"></i></a>
                </td>
                <td>
                  @if ($row->name1)
                  {!! ($row->name3 == 'Root' ? '' : '<strong>'.$row->name3 . '</strong> <i class="fa fa-angle-double-right" aria-hidden="true"></i> ') !!} <strong>{{ $row->name2 }}</strong> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <strong>{{ $row->name1 }}</strong>
                  @else

                  @endif
                </td>
                <td>{!! ez_status($row->status) !!}</td>
                <td>{{ $row->updated_at }}</td>
                <td><a href="{{ route('p.page.edit', ['id' => $row->id]) }}" class="btn btn-block btn-primary btn-xs">Edytuj <i class="fa fa-edit"></></a></td>
              </tr>
              @endforeach
              @endif


            </tbody>
          </table>
          {{ $q ? $pages->appends(['q' => $q])->links() : $pages->links()  }}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
