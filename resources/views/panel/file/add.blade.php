@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-9">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
        </div>
        {{ Form::model($file, [
            'route' => ['p.file.add'],
            'class' => 'form-horizontal',
            'files' => true
        ]) }}
        <div class="box-body">
          @include('panel.inc.errors')
          <div class="form-group">
            {{ Form::label('description', 'Opis*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('description', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('source', 'Źródło', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('source', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('status', 'Status*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('status', [
                1 => 'Aktywny',
                0 => 'Nieaktywny',
              ], null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('category', 'Kategoria*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('category', [
                \App\Models\File::CATEGORY_OTHER => 'Inne',
                \App\Models\File::CATEGORY_RESULT => 'Wyniki',
                \App\Models\File::CATEGORY_REGULATION => 'Regulaminy',
              ], 0, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group" >
            {{ Form::label('o_file', 'Plik*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::file('o_file', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="box-footer">
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
        <div class="box-footer">
          @include('panel.inc.form_required', ['form_required'=>true])
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

