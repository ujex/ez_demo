@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }} 
            <small class="">(Aby posortować po kolumnie "Opis", "Pobrania" lub "Data dodania", kliknij w nazwę kolumny)</small>
          </h3>
          
          <div class="box-tools pull-right">
            {{ Form::open(['route' => ['p.file.list'], 'method' => 'get']) }}
            <div class="has-feedback">
              {{ Form::text('q', $q, ['class' => 'form-control input-sm', 'placeholder' => 'Szukaj...', 'autocomplete' => 'off']) }}
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            {{ Form::close() }}
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive" >
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th title="Sortuj po opisie"><a href="{{ route('p.file.list', ['q' => $q, 's' => 'description', 'd' => ($d=='asc'?'desc':'asc')]) }}">Opis </a>
                  @if ($s == 'description')
                  <i class="fa fa-sort-{{ $d }}"></i>
                  @endif
                </th>
                <th>Link do pliku</th>
                <th title="Sortuj po ilości pobrań"><a href="{{ route('p.file.list', ['q' => $q, 's' => 'downloads', 'd' => ($d=='asc'?'desc':'asc')]) }}">Pobrania</a>
                  @if ($s == 'downloads')
                  <i class="fa fa-sort-{{ $d }}"></i>
                  @endif
                </th>
                <th>Typ</th>
                <th>Kategoria</th>
                <th style="width:130px;" title="Sortuj po dacie dodania"><a href="{{ route('p.file.list', ['q' => $q, 's' => 'created_at', 'd' => ($d=='asc'?'desc':'asc')]) }}">Data dodania</a>
                  @if ($s == 'created_at')
                  <i class="fa fa-sort-{{ $d }}"></i>
                  @endif
                </th>
              </tr>
            </thead>
            <tbody>
              @if ($files)
              @foreach ($files as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                  <i class="fa fa-copy" data-ez-content="{{ $row->description }}" data-toggle="tooltip" title="Kliknij aby skopiować opis do schowka"></i> 
                  <a href="{{ route('download', ['id' => $row->id, 'name' => $row->name, 'out' => 's']) }}" target="_blank">
                    <strong>{{ $row->description }}</strong> 
                  </a>
                </td>
                <td><i class="fa fa-copy" data-ez-content="{{ route('download', ['id' => $row->id, 'name' => $row->name, 'out' => 's']) }}" data-toggle="tooltip" title="Kliknij aby skopiować link do schowka"></i> 
                  {{ route('download', ['id' => $row->id, 'name' => $row->name, 'out' => 's']) }}
                </td>
                <td>

                  <span class="label label-info pull-right"><i class="fa fa-download"></i> {{ $row->downloads }} pobrań</span>
                  <span class="label label-success pull-right"><i class="fa fa-clock-o"></i> {{ $row->updated_at->diffForHumans() }}</span>
                </td>
                <td><i class="fa fa-lg {{ $row->extIco() }}" data-toggle="tooltip" title="{{ $row->ext }}"></i></td>
                <td>{{ $row->category }}</td>
                <td>{{ $row->created_at }}</td>
              </tr>
              @endforeach
              @endif


            </tbody>
          </table>
          {{ $files->appends(['q' => $q, 's' => $s, 'd' => $d])->links() }}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
<script>
    $(function () {
        $('.fa-copy').click(function () {
            $(this).ezCopyToClipboard();
            $('#flash').ezShowFlash({
                type: 'info',
                message: 'Treść została skopiowana do schowka.'
            });
        });

    });
</script>
@endsection