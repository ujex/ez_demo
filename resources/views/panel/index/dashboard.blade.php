@extends('layouts.backend')


@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
@endsection

@section('content')
<section class="content-header">
  <h1>{{ $title }}<small></small></h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $small_box['post'] }}</h3>
          <p>Artykułów</p>
        </div>
        <div class="icon">
          <i class="fa fa-file-text"></i>
        </div>
        <a href="" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $small_box['file'] }} <sup style="font-size: 20px">{{ $small_box['downloads'] }}</sup></h3>
          <p>Plików <sup>Łącznie pobrań</sup></p>
        </div>
        <div class="icon">
          <i class="fa fa-files-o"></i>
        </div>
        <a href="{{ route('p.file.list') }}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>0</h3>
          <p>Galerii</p>
        </div>
        <div class="icon">
          <i class="fa fa-file-photo-o"></i>
        </div>
        <a href="" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>0</h3>
          <p>Filmów</p>
        </div>
        <div class="icon">
          <i class="fa fa-file-video-o"></i>
        </div>
        <a href="" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  </div>
  <div class="row">

    <div class="col-md-6">
      @if ($files)
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Ostatnio pobierane pliki</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <ul class="todo-list">
            @foreach ($files as $row)
            <li>
              <span>
                <i class="fa {{ $row->extIco() }}"></i>
              </span>
              <span class="text"><a href="{{ route('download', ['id' => $row->id, 'name' => $row->name, 'out' => 's']) }}" target="_blank">{{ $row->description }}</a></span>
              <span class="label label-info pull-right"><i class="fa fa-download"></i> {{ $row->downloads }} pobrań</span>
              <span class="label label-success pull-right margin-r-5"><i class="fa fa-clock-o"></i> {{ $row->updated_at->diffForHumans() }}</span>
            </li>
            @endforeach
          </ul>
        </div>
        <div class="box-footer clearfix no-border">
          <a href="{{ route('p.file.list') }}" class="btn btn-default pull-left"><i class="fa fa-list"></i> Zobacz wszystkie</a>  
          <a href="{{ route('p.file.add') }}" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Dodaj plik</a>
        </div>
      </div>
      @endif
    </div>

    <div class="col-md-6">
      @if ($calendar)
      <div class="box box-solid">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Kalendarz</h3>
        </div>
        <div class="box-body no-padding">
          <div id="calendar" style="width: 100%"></div>
        </div>
        <div class="box-footer text-center text-black">
          <a href="{{ route('p.calendar.list') }}" class="btn btn-default pull-left"><i class="fa fa-list"></i> Zobacz wszystkie</a>  
          <a href="{{ route('p.calendar.add') }}" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Dodaj nowy</a>
        </div>
      </div>
      @endif
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <canvas id="salesChart" style="height: 180px;"></canvas>
    </div>
  </div>






</section>

@endsection


@section('page-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('assets/lte/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/chartjs/Chart.min.js') }}"></script>
<script>
$(function () {


var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
// This will get the first returned node in the jQuery collection.
var salesChart = new Chart(salesChartCanvas);
var salesChartData = {
labels  : [
@foreach ($registration as $k=>$v)
    '{{ $k }}',
@endforeach
],
    datasets: [
    {
    label               : 'Registered',
        fillColor           : 'rgb(123, 204, 233)',
        strokeColor         : 'rgb(123, 204, 233)',
        pointColor          : 'rgb(123, 204, 233)',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgb(123, 204, 233)',
        data                : [
@foreach ($registration as $k=>$v)
    '{{ $v }}',
@endforeach
        ]
    },
    ]
};
var salesChartOptions = {
// Boolean - If we should show the scale at all
showScale               : true,
    // Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines      : false,
    // String - Colour of the grid lines
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    // Number - Width of the grid lines
    scaleGridLineWidth      : 1,
    // Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    // Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines  : true,
    // Boolean - Whether the line is curved between points
    bezierCurve             : true,
    // Number - Tension of the bezier curve between points
    bezierCurveTension      : 0.3,
    // Boolean - Whether to show a dot for each point
    pointDot                : false,
    // Number - Radius of each point dot in pixels
    pointDotRadius          : 4,
    // Number - Pixel width of point dot stroke
    pointDotStrokeWidth     : 1,
    // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    // Boolean - Whether to show a stroke for datasets
    datasetStroke           : true,
    // Number - Pixel width of dataset stroke
    datasetStrokeWidth      : 2,
    // Boolean - Whether to fill the dataset with a color
    datasetFill             : true,
    // String - A legend template
    legendTemplate          : '',
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio     : true,
    // Boolean - whether to make the chart responsive to window resizing
    responsive              : true
};
// Create the line chart
salesChart.Line(salesChartData, salesChartOptions);



@if ($calendar)
    function ini_events(ele) {
    ele.each(function () {

    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
    // it doesn't need to have a start or end
    var eventObject = {
    title: $.trim($(this).text()) // use the element's text as the event title
    };
    // store the Event Object in the DOM element so we can get to it later
    $(this).data('eventObject', eventObject);
    });
    }

ini_events($('#external-events div.external-event'));
$('#calendar').fullCalendar({
header: {
left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek'
},
    buttonText: {
    today: 'today',
        month: 'month',
        week: 'week'
    },
    events: [
        @foreach ($calendar as $row)
    {
    title: '{{ $row->category->name }}: {{ $row->name }}',
        start: '{{ $row->date_start }}',
        @if ($row->date_end)
        end: '{{ $row->getEndDateToJs() }}',
        @endif
        url: '{{ route("p.calendar.edit", ["id" => $row->id]) }}',
        textColor: '{{ $row->getTextColor() }}',
        backgroundColor: '{{ $row->getBgColor() }}',
        borderColor: '{{ $row->category->color }}'
    },
        @endforeach
    ]
});
@endif

    });
</script>
@endsection