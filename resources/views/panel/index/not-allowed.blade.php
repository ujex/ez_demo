@extends('layouts.backend')

@section('content')
<section class="content">
  <div class="row">
      <div class="col-md-12">
          
      
  <div class="content">
    <section class="content-header">
      <h1>{{ $title or 'Błąd' }}</h1>
    </section>

    <section class="content">
      <div class="error-page">
        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Nie posiadasz uprawnień do wykonywania tej akcji!</h3>
          <p>
              <a href="/panel/dashboard">Wróć do strony głównej </a>
          </p>
        </div>
      </div>
    </section>
  </div>
          
</div>
</div>
</section>
@endsection
