@extends('layouts.backend')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/select2/select2.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <div class="row">
            <div class="box-body">
              <h3 class="box-title">{{ $title }}</h3>
            </div>
          </div>
          {{ Form::open(['route' => ['p.calendar.list']]) }}
          <div class="row">
            <div class="box-body">
              <div class="col-xs-3">
                {{ Form::text('q', $q, ['class' => 'form-control input-sm', 'placeholder' => 'Szukaj...', 'autocomplete' => 'off']) }} 
              </div>
              <div class="col-xs-3">
                {{ Form::select('q_c', [0=>'Dyscyplina'] + $category, $q_c, ['class' => 'form-control input-sm select2']) }} 
              </div>
              <div class="col-xs-3">
                {{ Form::select('q_s', $seasons, $q_s, ['class' => 'form-control input-sm']) }} 
              </div>
              <div class="col-xs-2">
                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"> </i> Szukaj</button>
              </div>
            </div>
          </div>
          {{ Form::close() }}
        </div>
        <div class="box-body table-responsive">
          <table class="table table-bordered table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Nazwa</th>
                <th style="width:153px;">Data</th>
                <th>Miejsce</th>
                <th>Organizator</th>
                <th>Dyscyplina</th>
                <th>Regulamin/Wyniki</th>
              </tr>
            </thead>
            <tbody>
              @if ($calendar)
              @foreach ($calendar as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                  <a href="{{ route('p.calendar.edit', ['id' => $row->id]) }}">
                    <strong title="{{ $row->name }}" data-toggle="tooltip">{{ $row->name }}</strong>
                  </a>
                </td>
                <td>{{ $row->date_start }} {{ ($row->date_end ? ' - ' . $row->date_end : '') }}</td>
                <td>{{ $row->place }}</td>
                <td>{{ $row->organiser }}</td>
                <td>{{ $row->category->name }}</td>
                <td>
                    @if ($row->reg->count())
                    <span class="label label-success">TAK</span>
                    @else
                    <span class="label label-danger">NIE</span>
                    @endif
                    /
                    @if ($row->res->count())
                    <span class="label label-success">TAK</span>
                    @else
                    <span class="label label-danger">NIE</span>
                    @endif
                </td>
              </tr>
              @endforeach
              @endif


            </tbody>
          </table>
          {{ $calendar->links() }}

        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/select2/select2.full.min.js') }}"></script>
<script>
$(function () {
    $(".select2").select2();

});
</script>
@endsection