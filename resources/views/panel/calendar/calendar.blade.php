@extends('layouts.backend')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
        </div>
        <div class="box-body">
          <div id="calendar"></div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('assets/lte/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script>
$(function () {



    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

      });
    }

    ini_events($('#external-events div.external-event'));
    $('#calendar').fullCalendar({
      height: 650,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week: 'week'
      },
      events: [
        @foreach ($calendar as $row)
        {
          title: '{{ $row->category->name }}: {{ $row->name }}',
          start: '{{ $row->date_start }}',
          @if ($row->date_end)
          end: '{{ $row->getEndDateToJs() }}',    
          @endif
          url: '{{ route("p.calendar.edit", ["id" => $row->id]) }}'
        },
        @endforeach
      ]
    });

});
</script>
@endsection