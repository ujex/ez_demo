@extends('layouts.backend')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/select2/select2.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-9">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
        </div>
        {{ Form::model($competition, [
            'route' => ['p.calendar.add'],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          @include('panel.inc.errors')
          <div class="form-group">
            {{ Form::label('name', 'Nazwa*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date_start', 'Data start*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date_start', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date_end', 'Data koniec', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date_end', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('place', 'Miejsce*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('place', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('lat', 'Szerokość geograficzna (Lat)', ['class' => 'col-sm-4 control-label']) }}
            <div class="col-sm-8">{{ Form::text('lat', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('lng', 'Długość geograficzna (Lng)', ['class' => 'col-sm-4 control-label']) }}
            <div class="col-sm-8">{{ Form::text('lng', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('organiser', 'Organizator', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('organiser', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('category_id', 'Dyscyplina*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('category_id', $category, null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('ctgs', 'Kategorie', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              @foreach ($ctg as $k=>$v)
              <label class=" label-default">&nbsp;&nbsp;&nbsp; 
                {{ Form::checkbox('ctg_id['.$k.']', $k, null) }} {{ $v }} &nbsp;&nbsp;&nbsp;</label>&nbsp;&nbsp;&nbsp;
              @endforeach
            </div>
          </div>

          <div class="box-footer">
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
        <div class="box-footer">
          @include('panel.inc.form_required', ['form_required'=>true])
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/select2/select2.full.min.js') }}"></script>
<script>
$(function () {
    $(".select2").select2();

    $('#date_start').datepicker({
        dateFormat: 'yy-mm-dd',
        autoclose: true
    });
    $('#date_end').datepicker({
        dateFormat: 'yy-mm-dd',
        autoclose: true
    });
});
</script>
@endsection