@extends('layouts.backend')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/select2/select2.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $title }}</h3>
        </div>
        {{ Form::model($competition, [
            'route' => ['p.calendar.edit', 'id' => $competition->id],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          @include('panel.inc.errors')
          <div class="form-group">
            {{ Form::label('name', 'Nazwa*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('code', 'Code*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('code', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date_start', 'Data start*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date_start', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date_end', 'Data koniec', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date_end', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('registration_to', 'Zgłoszenia do', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('registration_to', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('register_form', 'Typ formularz', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('register_form', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('place', 'Miejsce*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('place', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('lat', 'Szerokość geograficzna (Lat)', ['class' => 'col-sm-4 control-label']) }}
            <div class="col-sm-8">{{ Form::text('lat', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('lng', 'Długość geograficzna (Lng)', ['class' => 'col-sm-4 control-label']) }}
            <div class="col-sm-8">{{ Form::text('lng', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('organiser', 'Organizator', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('organiser', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('category_id', 'Dyscyplina*', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('category_id', $category, null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('ctg', 'Kategorie', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              @foreach ($ctg as $k=>$v)
              <label class="{{ in_array($k, $ctg_curr) ? 'label-info' : 'label-default' }}">&nbsp;&nbsp;&nbsp; 
                {{ Form::checkbox('ctg_id['.$k.']', $k, in_array($k, $ctg_curr) ? true : false, null) }} {{ $v }} &nbsp;&nbsp;&nbsp;
              </label>&nbsp;&nbsp;&nbsp;
              @endforeach
            </div>
          </div>
          <div class="form-group">
            {{ Form::label('info', 'Info', ['class' => 'col-sm-1 control-label']) }}
            <div class="col-sm-11">{{ Form::textArea('info', null, ['class' => 'form-control']) }}</div>
          </div>
          
          
          
          <div class="box-footer">
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
        <div class="box-footer">
          @include('panel.inc.form_required', ['form_required'=>true])
        </div>

      </div>
    </div>

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-body box-profile">
          <a href="{{ route('l.competition.show', ['slug' => $competition_info->url, 'loc' => 'pl']) }}" target="_blank" class="text-red">Zobacz w serwisie <i class="glyphicon glyphicon-new-window"></i></a>
          @if (Entrust::can('p.calendar') )
          <div class="box-footer">
            <a class="btn btn-danger pull-left confirm" href="{{ route('p.calendar.edit', ['id' => $competition_info->id, 'delete' => true, 'name' => $competition_info->name]) }}">Usuń wyścig</a>
            <br style="clear:both;" />
            <small class="label-danger">Uwaga! Usunięcie wyścigu nie może zostać cofnięte.</small>
          </div>
          @endif
        </div>
      </div>
      @if (Entrust::can('p.calendar'))
      <div class="box box-warning">
        <div class="box-header">
          <h4>Regulamin</h4>
        </div>
        <div class="box-body box-profile">
          @if ($competition_info->reg->count())
          <ul class="list-unstyled">
            @foreach ($competition_info->reg as $file)
            <li>
              <a href="{{ route('p.calendar.edit.relation', ['id' => $competition_info->id, 'file_id' => $file->id, 'unlink' => true]) }}" class="text-red confirm" data-toggle="tooltip" title="Usuń plik"><i class="glyphicon glyphicon-remove-circle"></i></a> 
              <a href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}" target="_blank">{{ $file->description }}</a>
            </li>
            @endforeach
          </ul>
          @else
          <p><strong>Brak</strong></p>
          @endif
        </div>
        <div class="box-footer">
          {{ Form::model($competition, [
                'route' => ['p.calendar.edit.relation', 'id' => $competition->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) }}
          <p>Dodaj regulamin:</p>
          <p>{{ Form::text('desc', null, ['class' => 'form-control', 'placeholder' => 'opis...']) }}</p>
          <p>{{ Form::file('reg_file', null, ['class' => 'form-control', 'placeholder' => 'wybierz plik']) }}</p>
          {{ Form::hidden('is_reg_form', 1) }}
          <p>{{ Form::submit('Prześlij', ['class' => 'btn btn-warning pull-right']) }}</p>
          {{ Form::close() }}
        </div>
      </div>
      @endif
      @if (Entrust::can('p.calendar') || Entrust::can('p.calendar.result'))
      <div class="box box-success">
        <div class="box-header">
          <h4>Wyniki</h4>
        </div>
        <div class="box-body box-profile">
          @if ($competition_info->res->count())
          <ul class="list-unstyled">
            @foreach ($competition_info->res as $file)
            <li>
              <a href="{{ route('p.calendar.edit.relation', ['id' => $competition_info->id, 'file_id' => $file->id, 'unlink' => true]) }}" class="text-red confirm" data-toggle="tooltip" title="Usuń plik"><i class="glyphicon glyphicon-remove-circle"></i></a> 
              <a href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}" target="_blank">{{ $file->description }}</a>
            </li>
            @endforeach
          </ul>
          @else
          <p><strong>Brak</strong></p>
          @endif
        </div>
        <div class="box-footer">
          {{ Form::model($competition, [
                'route' => ['p.calendar.edit.relation', 'id' => $competition->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) }}
          <p>Dodaj wyniki:</p>
          <p>{{ Form::text('desc', null, ['class' => 'form-control', 'placeholder' => 'opis...']) }}</p>
          <p>{{ Form::file('res_file', null, ['class' => 'form-control', 'placeholder' => 'wybierz plik']) }}</p>
          {{ Form::hidden('is_res_form', 1) }}
          <p>{{ Form::submit('Prześlij', ['class' => 'btn btn-success pull-right']) }}</p>
          {{ Form::close() }}
        </div>
      </div>
      @endif
      <div class="box box-info">
        <div class="box-header">
          <h4>Geolokalizacja</h4>
        </div>
        <div class="box-body box-profile">
          <div class="embed-responsive embed-responsive-4by3">
            <div id="map" class="embed-responsive-item"></div>
          </div>
        </div>
        @if (Entrust::can('p.calendar'))
        <div class="box-footer">
          <a class="btn btn-info pull-left confirm" href="{{ route('p.calendar.edit', ['id' => $competition_info->id, 'geolocation' => true]) }}">Pobierz geolokalizację na podstawie miejsca</a>
          <br style="clear:both;" />
          <small class="label-danger">Uwaga! Pobranie geolokalizacji spowoduje nadpisanie bierzących wartości szerokości i długości geograficznej.</small>
        </div>
        @endif
      </div>
    </div>

  </div>
  
  
@if ($competition->players->count())
<a href="{{ route('p.print.players', ['id' => $competition_info->id]) }}">Pobierz</a>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <div class="row">
            <div class="box-body">
              <h3 class="box-title">Zgłoszenia</h3>
            </div>
          </div>
        </div>
        <div class="box-body table-responsive">
          <table class="table table-bordered table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Nazwisko</th>
                <th>Imię</th>
                <th>Data ur.</th>
                <th>UCI ID</th>
                <th>Team</th>
                <th>Telefon</th>
                <th>e-mail</th>
                <th>Data</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($competition->players as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $row->surname }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->birth_date }}</td>
                <td>{{ $row->uci_id }}</td>
                <td>{{ $row->team }}</td>
                <td>{{ $row->phone }}</td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->created_at }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endif
  
  
  
  
  
</section>
@endsection

@section('page-script')
@include('panel.inc.tinymce')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/select2/select2.full.min.js') }}"></script>
<script>
$(function () {
    $(".select2").select2();

    $('#date_start').datepicker({
        dateFormat: 'yy-mm-dd',
        autoclose: true
    });
    $('#date_end').datepicker({
        dateFormat: 'yy-mm-dd',
        autoclose: true
    });
});
@if ($competition_info->lat && $competition_info->lng)
function initMap() {
    var myLatLng = {lat: {{ $competition_info->lat }}, lng: {{ $competition_info->lng }}};
    var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        scrollwheel: false,
        zoom: 11
    });
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
    });
}
@endif
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ config('ez.google_api_key') }}&callback=initMap">
</script>
@endsection