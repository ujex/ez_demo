@extends('layouts.frontend')

@section('page-css')
@endsection


@section('content')
<h1>{{ __('site.comp.last') }}</h1>
<ul class="list-unstyled">
  @foreach ($comp as $comp)
  <li>
    <span class="g">{{ $comp->date_start->format('d.m.Y') }} {{ $comp->date_end ? '- '.$comp->date_end->format('d.m.Y'):'' }} &bull; <a href="/wyniki/0,przelaj/">{{ $comp->category->name }}</a></span><br />
    <h3><a href="{{ route('l.competition.show', ['slug' => $comp->url, 'loc' => $loc]) }}" class="green">
        {{ $comp->name }}
      </a></h3>
  </li>
  @endforeach
</ul>
@endsection


@section('page-script')
@endsection