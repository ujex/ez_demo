<nav class="navbar navbar-default navbar-fixed-top menu" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('l.home', ['loc' => $loc]) }}">
        <img src="{{ asset('images/logo.png') }}" alt="e-zawody.pl" />
      </a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav mnav">
        <li style="padding-left:20px;"><a href="{{ route('l.competition.list', ['loc' => $loc]) }}">{{ __('site.menu.results') }}</a></li>
        <li><a href="{{ route('l.page.show', ['loc' => $loc, 'id' => 1, 'slug' => 'offer']) }}">{{ __('site.menu.offer') }}</a></li>
        <li><a href="{{ route('l.page.show', ['loc' => $loc, 'id' => 2, 'slug' => 'contact']) }}">{{ __('site.menu.contact') }}</a></li>
        
        @if (!Auth::guest())
        <li><a href="{{ route('p.home') }}" class="uppercase">Panel</a></li>
        @endif
      </ul>

      <ul class="nav navbar-nav navbar-right mnav">
        <li class="dropdown menu-item">
          <a href="" class="dropdown-toggle" data-toggle="dropdown">{!! ez_flag($loc) !!} {{ ez_uc($loc) }}<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('l.home', ['loc' => 'en']) }}">{!! ez_flag('en') !!} EN</a></li>
            <li><a href="{{ route('l.home', ['loc' => 'pl']) }}">{!! ez_flag('pl') !!} PL</a></li>
          </ul>
        </li>
      </ul>
@if (0)
      <ul class="nav navbar-nav navbar-right mnav">
        @if (Auth::check())
        <li><a href="" class="sign">{{ __('site.menu.sign') }}</a></li>
        @endif
        <li class="dropdown">
          @if (Auth::check())
          <a href="" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span> {{ __('site.menu.loggedas') }}: {{ Auth::user()->name }} {{ Auth::user()->surname }}<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="">{{ __('site.menu.account') }}</a></li>
            @if (Entrust::can('p.home'))
            <li><a href="{{ route('p.home') }}">Panel</a></li>
            <li><a href="{{ route('l.a.player.list', ['loc' => $loc]) }}">{{ __('site.menu.player_list') }}</a></li>
            @endif
            <li class="divider"></li>
            <li><a href="{{ url('/logout') }}">{{ __('site.menu.logout') }}</a></li>
          </ul>
          @else
          <a href="" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span> {{ __('site.menu.account') }}<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ route('l.login', ['loc' => $loc]) }}">{{ __('site.menu.login') }}</a></li>
            <li><a href="{{ route('l.register', ['loc' => $loc]) }}">{{ __('site.menu.register') }}</a></li>
          </ul>
          @endif
        </li>
      </ul>
@endif
    </div>
  </div>
</nav>