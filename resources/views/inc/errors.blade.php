@if (count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <div class="col-sm-6">{{ $error }}</div>
        @endforeach
        <br style="clear:both;">
    </div>
@endif

@if(Session::has('info'))
    <div class="alert alert-info">
        {{ Session::get('info') }}
    </div>
@endif