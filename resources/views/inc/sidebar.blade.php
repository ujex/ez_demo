<div class="short-box">
  <h4>{{ __('site.comp.last') }}</h4>
  <ul class="list-unstyled">
    @if (isset($comp_last) && $comp_last)
    @foreach ($comp_last as $comp)
    <li><span>{{ $comp->date_start->format('d.m.Y') }} {{ $comp->date_end ? '- '.$comp->date_end->format('d.m.Y'):'' }}</span><br />
      <a href="{{ route('l.competition.show', ['slug' => $comp->url, 'loc' => $loc]) }}">
        {{ $comp->name }}
      </a></li>
    @endforeach
    @endif
  </ul>
</div>

<div class="short-box">
  <h4>{{ __('site.comp.next') }}</h4>
  <ul class="list-unstyled">
    @if (isset($comp_next) && $comp_next)
    @foreach ($comp_next as $comp)
    <li><span>{{ $comp->date_start->format('d.m.Y') }} {{ $comp->date_end ? '- '.$comp->date_end->format('d.m.Y'):'' }}</span><br />
      <a href="{{ route('l.competition.show', ['slug' => $comp->url, 'loc' => $loc]) }}">
        {{ $comp->name }}
      </a></li>
    @endforeach
    @endif
  </ul>
</div>

<div class="short-box">
<div class="fb-page" data-href="https://www.facebook.com/ezawody/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ezawody/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ezawody/">e-zawody.pl</a></blockquote></div>
</div>