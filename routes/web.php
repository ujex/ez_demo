<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/redirect', 'Auth\SocialAuthController@fb_redirect');
Route::get('/callback', 'Auth\SocialAuthController@fb_callback');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');


Route::group([
    'middleware' => ['localize'],
], function (){
    Route::get('/', 'HomeController@indexAction')->name('home');
});

Route::get('ajax/result/{id}', 'CompetitionController@showAjaxAction')->name('ajax.result');

Route::group([
    'prefix' => '{loc}',
    'middleware' => ['localize'],
    'where' => array('loc' => '[a-z]{2}')
], function (){
    Route::get('/', 'HomeController@indexAction')->name('l.home');
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('l.login');
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('l.register');
    
    Route::get('home', 'HomeController@indexAction')->name('l.home.h');
    Route::get('r', 'HomeController@testAction')->name('l.home.w');
    
    // Single pages
    Route::get('/{id},{slug}.html', 'PageController@showAction')->name('l.page.show')->where(['id' => '[0-9]+', 'slug' => '[A-Za-z0-9 _ .-]+']);
    Route::get('/{slug}.html', 'CompetitionController@showAction')->name('l.competition.show')->where(['slug' => '[A-Za-z0-9 _ .-]+']);
    Route::get('/result-{code}', 'CompetitionController@resAction')->name('l.competition.res')->where(['code' => '[A-Za-z0-9 _ .-]+']);

    Route::get('/results/{slug?}', 'CompetitionController@listAction')->name('l.competition.list')->where(['slug' => '[A-Za-z0-9 _ .-]+']);
    
    Route::match(['GET', 'POST'], '/sign-{code}', 'RegisterController@signAction')->name('l.register.sign')->where(['code' => '[A-Za-z0-9 _ .-]+']);
    
    //Acc
    Route::get('acc/players', 'Acc\PlayerController@listAction')->name('l.a.player.list');
    Route::get('acc/players/add', 'Acc\PlayerController@addAction')->name('l.a.player.add');
});


// File download
Route::get('/pobierz/{id}/{out?}/{name?}', 'FileController@downloadAction')->name('download')->where(['id' => '[0-9]+', 'name' => '[A-Za-z0-9 _ .-]+', 'out' => '[a-z]{1}']);


// Admin
Route::group(['prefix' => 'panel'], function (){
    Route::get('dashboard', 'Panel\IndexController@dashboardAction')->name('p.home')->middleware('perm:p.home');

    // Users
    Route::get('user', 'Panel\UserController@listAction')->name('p.user.list')->middleware('perm:p.user');
    Route::match(['GET', 'POST'], 'user/add', 'Panel\UserController@addAction')->name('p.user.add')->middleware('perm:p.user');
    Route::match(['GET', 'POST'], 'user/edit/{id}', 'Panel\UserController@editAction')->name('p.user.edit')->middleware('perm:p.user');
    Route::get('user/role', 'Panel\UserController@roleAction')->name('p.user.role')->middleware('perm:p.user.role');
    Route::get('user/role/edit/{id}', 'Panel\UserController@roleEditAction')->name('p.user.role.edit')->middleware('perm:p.user.role');
    Route::get('user/permision', 'Panel\UserController@permissionAction')->name('p.user.permission')->middleware('perm:p.user.role');
    Route::post('user/permision/add', 'Panel\UserController@permissionAddAction')->name('p.user.permission.add')->middleware('perm:p.user.role');

    // Calendar
    Route::match(['GET', 'POST'], 'calendar/list', 'Panel\CalendarController@listAction')->name('p.calendar.list')->middleware('perm:p.calendar,p.calendar.commissarie,p.calendar.result');
    Route::get('calendar', 'Panel\CalendarController@calendarAction')->name('p.calendar')->middleware('perm:p.calendar,p.calendar.commissarie,p.calendar.result');
    Route::match(['GET', 'POST'], 'calendar/add', 'Panel\CalendarController@addAction')->name('p.calendar.add')->middleware('perm:p.calendar');
    Route::match(['GET', 'POST'], 'calendar/edit/{id}', 'Panel\CalendarController@editAction')->name('p.calendar.edit')->middleware('perm:p.calendar,p.calendar.commissarie,p.calendar.result');
    Route::match(['GET', 'POST'], 'calendar/edit/relation/{id}', 'Panel\CalendarController@editRelationAction')->name('p.calendar.edit.relation')->middleware('perm:p.calendar');
    
    Route::get('print/players/{id}', 'Panel\CalendarController@playersAction')->name('p.print.players')->middleware('perm:p.calendar');
    
    // Pages
    Route::get('page', 'Panel\PageController@listAction')->name('p.page.list')->middleware('perm:p.page,p.page.commissarie,p.page.tor');
    Route::match(['GET', 'POST'], 'page/add', 'Panel\PageController@addAction')->name('p.page.add')->middleware('perm:p.page');
    Route::match(['GET', 'POST'], 'page/edit/{id}', 'Panel\PageController@editAction')->name('p.page.edit')->middleware('perm:p.page,p.page.commissarie,p.page.tor');
    
    // Notes
    Route::get('note', 'Panel\NoteController@listAction')->name('p.note.list')->middleware('perm:p.note');
    Route::match(['GET', 'POST'], 'note/edit/{id}', 'Panel\NoteController@editAction')->name('p.note.edit')->middleware('perm:p.note');
    
    // File Manager
    Route::get('file', 'Panel\FileController@listAction')->name('p.file.list')->middleware('perm:p.file');
    Route::match(['GET', 'POST'], 'file/add', 'Panel\FileController@addAction')->name('p.file.add')->middleware('perm:p.file');
    
    //Not allowed
    Route::get('user/not-allowed', 'Panel\IndexController@notAllowedAction')->name('p.not-allowed');
});


Route::group(['namespace' => 'Api'], function() {
    Route::get('/api/player/search/{type}', 'PlayerController@searchAction');

});


Route::get('/{r}/{s?}/{t?}', 'HomeController@redirAction');