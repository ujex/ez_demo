// https://www.npmjs.com/package/ftp-deploy
var FtpDeploy = require('ftp-deploy');
var ftpDeploy = new FtpDeploy();
 
var config = {
    username: "ezawody",
    password: "plrbj2181", // optional, prompted if none given 
    host: "ftp.ezawody.ayz.pl",
    port: 21,
    localRoot: __dirname + "/public",
    remoteRoot: "/domains/test/",
    include: ['js/*', 'css/*'],
    exclude: ['.git', 'assets/*', 'flags/*', 'fonts/*', 'images/*', 'storage/*', '.htaccess', 'index.php', 'favicon.ico', 'mix-manifest.json', 'robots.txt', 'webconfig']
}
    
ftpDeploy.deploy(config, function(err) {
    if (err) console.log(err)
    else console.log('finished');
});
ftpDeploy.on('uploading', function(data) {
    data.totalFileCount;       // total file count being transferred 
    data.transferredFileCount; // number of files transferred 
    data.percentComplete;      // percent as a number 1 - 100 
    data.filename;             // partial path with filename being uploaded 
});
ftpDeploy.on('uploaded', function(data) {
    console.log(data);         // same data as uploading event 
});