<?php
namespace App\Services;

use App\Models\SocialAccount;
use App\Models\User;
use Laravel\Socialite\Contracts\User as ProviderUser;
class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            $account->avatar = $providerUser->avatar;
            $account->nickname = $providerUser->nickname;
            $account->profile_url = $providerUser->profileUrl;
            $account->gender = (isset($providerUser->user['gender']) ? $providerUser->user['gender'] : null);
            $account->save();
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook',
                'avatar' => $providerUser->avatar,
                'nickname' => $providerUser->nickname,
                'profile_url' => $providerUser->profileUrl,
                'gender' => (isset($providerUser->user['gender']) ? $providerUser->user['gender'] : null),
            ]);
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
                $name = explode(' ', $providerUser->getName());
                
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $name[0],
                    'surname' => $name[1],
                    'status' => 1,
                    'type' => 'U',
                    'password' => bcrypt(rand(1,10000).time()),
                ]);
            }else{
                $user->status = 1;
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }
}