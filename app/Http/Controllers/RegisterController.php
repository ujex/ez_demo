<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Competition;

class RegisterController extends FrontController
{

    public function signAction(Request $request)
    {
        $competition = Competition::with('reg', 'category', 'players')
        ->where('code', $request->code)
        ->first();
        
        if(!$competition){
            abort(404);
        }
        $errors = null;
        $player = new Player($request->all());
        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule());
            if (!$request->medical){
                unset($player->medical);
            }
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $player->name = title_case($request->name);
                $player->surname = title_case($request->surname);
                $player->team = ez_uc($request->team);
                $player->birth_date = date('Y-m-d', strtotime($request->birth_date));
                $player->phone = ez_phone($request->phone);
                
                if (!$request->uci_id){ $player->uci_id = null; }
                if (!$request->address){ $player->address = null; }
                if (!$request->phone){ $player->phone = null; }
                if (!$request->email){ $player->email = null; }
                if (!$request->team){ $player->team = null; }
                
                $player->competition_id = $competition->id;
                $player->save();

                $request->session()->flash('info', __('site.reg.saved'));
                return redirect()->route('l.register.sign', ['code'=>$competition->code, 'loc'=>\App::getLocale()]);
            }
        }

        return view('register.sign', [
            'competition' => $competition,
            'player' => $player,
            'errors' => $errors,
            'country' => \App\Models\Country::getMap(),
            'title' => 'Zapisy do '.$competition->name.', '.$competition->place.' '.$competition->date_start->format('d.m.Y'),
        ]);
    }

    public function listAction(Request $request)
    {
        $query = Competition::with('category')
        ->where('date_start', '<=', date('Y-m-d'))
        ->orderBy('date_start', 'desc')
        ;
        if ($request->slug){
            $query->whereHas('category', function($q) use($request){
                $q->where('url', $request->slug);
            });
        }
        $comp = $query->get();

        return view('competition.list', [
            'comp' => $comp,
            'title' => 'Wyniki',
        ]);
    }
    
    private function rule()
    {
        return [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'birth_date' => 'required|date',
            'email' => 'required|email',
            'phone' => 'required|max:64',

            'uci_id' => 'nullable|numeric|digits:11',
            'regulation' => 'required',
        ];
    }
}
