<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Jobs\SendVerificationEmail;
use App\Models\Page;

class PageController extends FrontController
{

    public function showAction(Request $request)
    {
        $page = Page::where('url', $request->slug)
        ->where('lang', $request->loc)
        ->first();
        
        if(!$page){
            abort(404);
        }

        return view('page.show', [
            'page' => $page,
            'title' => $page->title
        ]);
    }


}
