<?php

namespace App\Http\Controllers\Acc;

class AccController extends \App\Http\Controllers\Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        \Carbon\Carbon::setLocale(config('app.locale'));
    }

}
