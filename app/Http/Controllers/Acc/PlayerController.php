<?php

namespace App\Http\Controllers\Acc;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Team;

class PlayerController extends AccController
{

    public function listAction(Request $request)
    {
        $query = Player::where('user_id', \Auth::user()->id)
        ->orderBy('surname', 'asc')->orderBy('name', 'asc');

        if ($request->q){
            $q = ($request->q);
            $query->where(function($query) use($q){
                $query->where('surname', 'like', '%' . $q . '%')
                ->orWhere('name', 'like', '%' . $q . '%')
                ->orWhere('name_clean', 'like', '%' . $q . '%')
                ->orWhere('email', 'like', '%' . $q . '%')
                ->orWhere('phone', 'like', '%' . $q . '%');
            });
        }

        $players = $query->get();
        return view('acc.player.list', [
            'players' => $players,
            'q' => $request->get('q', null),
            'title' => 'Konfiguracja'
        ]);
    }
    
    public function addAction()
    {
        
        return view('acc.player.add', [

            'title' => 'Konfiguracja'
        ]);
    }

    private function rule_edit()
    {
        return [
            'code' => 'required|max:128',
        ];
    }

}
