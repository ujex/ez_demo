<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;

class FileController extends Controller
{

    public function __construct()
    {
        
    }

    public function downloadAction(Request $request)
    {
        $file = File::active()->where('id', $request->id)->where('name', $request->name)->first();
        if (!$file){
            abort(404);
        }
        #echo $file->downloads.'<br>';
        $file->downloads += 1;
        #echo $file->downloads;
        $file->save();
        #ez_log($request, 'download_file', $file->downloads, 'test');

        if ($request->out == 's'){
            return \Response::file(ez_storage($file->path) . $file->name);
        }
        return \Response::download(ez_storage($file->path) . $file->name, $file->name);
    }

}
