<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\Services\SocialFacebookAccountService;

class SocialAuthController extends Controller
{

    public function fb_redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function fb_callback(SocialFacebookAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
        auth()->login($user, true);
        
        return redirect()->to(route('l.home', ['loc' => session('locale')]));
    }

}
