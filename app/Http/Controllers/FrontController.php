<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use Jenssegers\Agent\Agent;
use App\Models\Note;
use App\Models\Competition;

class FrontController extends Controller
{

    
    public function __construct(Request $request)
    {
        $comp_last = \Cache::remember('fc_comp_last', 300, function(){
            return Competition::where('date_start', '<=', date('Y-m-d'))
            ->orderBy('date_start', 'desc')
            ->limit(5)->get();
        });
        $comp_next = \Cache::remember('fc_comp_next', 300, function(){
            return Competition::where('date_start', '>', date('Y-m-d'))
            ->orderBy('date_start', 'asc')
            ->limit(5)->get();
        });
        
        view()->composer('inc.sidebar', function ($view) use($comp_next, $comp_last){
            $view->with([
                'comp_next' => $comp_next,
                'comp_last' => $comp_last,
            ]);
        });
    }
}
