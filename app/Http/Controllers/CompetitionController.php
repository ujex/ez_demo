<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Rally;
use App\Jobs\SendVerificationEmail;
use App\Models\Competition;

class CompetitionController extends FrontController
{

    public function showAction(Request $request)
    {
        $competition = Competition::with('reg', 'res', 'category', 'rally')
        ->where('url', $request->slug)
        ->first();
        
        if(!$competition){
            abort(404);
        }

        return view('competition.show', [
            'competition' => $competition,
            'title' => 'Wyniki '.$competition->name.', '.$competition->place.' '.$competition->date_start->format('d.m.Y'),
        ]);
    }

    public function listAction(Request $request)
    {
        $query = Competition::with('category')
        ->where('date_start', '<=', date('Y-m-d'))
        ->orderBy('date_start', 'desc')
        ;
        if ($request->slug){
            $query->whereHas('category', function($q) use($request){
                $q->where('url', $request->slug);
            });
        }
        $comp = $query->get();

        return view('competition.list', [
            'comp' => $comp,
            'title' => 'Wyniki',
        ]);
    }

    public function resAction(Request $request)
    {
        $competition = Competition::with('reg', 'res', 'category', 'rally')
        ->where('code', $request->code)
        ->first();
        
        if(!$competition){
            abort(404);
        }

        return view('competition.res', [
            'competition' => $competition,
            'title' => 'Przeglądaj Wyniki '.$competition->name.', '.$competition->place.' '.$competition->date_start->format('d.m.Y'),
        ]);
    }
    
    public function showAjaxAction(Request $request)
    {
        $rally = Rally::with('result')
            ->where('status', 1)
        ->where('id', $request->id)
        ->first();
        
        if(!$rally){
            abort(404);
        }

        return view('competition.showAjax', [
            'rally' => $rally,
        ]);
    }
}
