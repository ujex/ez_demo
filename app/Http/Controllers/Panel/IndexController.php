<?php

namespace App\Http\Controllers\Panel;

use App\Models\File;
use App\Models\User;
use App\Models\Competition;
use App\Models\Player;

class IndexController extends BackController
{

    public function dashboardAction()
    {
        // Files
        $files = null;
        $files = File::orderBy('updated_at', 'desc')
            ->paginate(7);

        // Files
        $calendar = Competition::with('category')
            ->orderBy('date_start', 'asc')
            ->paginate(400);
        
        // Registration
        $start = time() - 60*60*24*14;
        $registration = [];
        for($i=1; $i<=15; $i++){
            $date1 = date('Y-m-d', $start);
            $date2 = date('d.m', $start);
            $registration[$date2] = Player::where('created_at', 'like', '%' . $date1 . '%')->get()->count();
            $start += (60*60*24);
        }


        return view('panel.index.dashboard', [
            'files' => $files,
            'calendar' => $calendar,
            'registration' => $registration,
            'small_box' => [
                'post' => 0,
                'file' => File::where('status', 1)->count(),
                'downloads' => File::where('status', 1)->sum('downloads'),
                'user' => User::where('type', 'A')->count(),
                'competition' => Competition::where('season', date('Y'))->count(),
            ],
            'title' => 'Panel Administracyjny'
        ]);
    }

    public function notAllowedAction()
    {
        return view('panel.index.not-allowed', [
            'title' => 'Brak uprawnień'
        ]);
    }

}
