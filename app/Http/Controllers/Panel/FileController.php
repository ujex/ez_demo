<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\File;

class FileController extends BackController
{

    public function listAction(Request $request)
    {
        if ($request->q){
            $q = str_slug($request->q);
            $query = File::where('name', 'like', '%' . $q . '%')
            ->orWhere('name_original', 'like', '%' . $q . '%')
            ->orWhere('description', 'like', '%' . $q . '%');
        }else{
            $query = File::where('id', '>', 0);
        }
        if($request->s){
            $query->orderBy($request->s, $request->d ? $request->d : 'asc');
        }else{
            $query->orderBy('created_at', 'desc');
        }
        $files = $query->paginate(config('ez.pagination.global'));

        return view('panel.file.list', [
            'files' => $files,
            'q' => $request->get('q', null),
            's' => $request->get('s', null),
            'd' => $request->get('d', null),
            'title' => 'Lista plików'
        ]);
    }

    public function addAction(Request $request)
    {
        $errors = null;
        $file = new File($request->all());

        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $file->user_id = \Auth::user()->id;
                $file->save();
                // File uploading
                if ($file_upload = $request->file('o_file')){
                    $file_name = $file->id . '_' . str_limit(str_slug($file->description, '_'), 33, '') . '.' . strtolower($file_upload->getClientOriginalExtension());
                    $file->path = 'pliki/' . date('Y') . '/';
                    $file_upload->move(ez_storage($file->path), $file_name);
                    $file->name = $file_name;
                    $file->name_original = $file_upload->getClientOriginalName();
                    $file->ext = strtolower($file_upload->getClientOriginalExtension());
                    $file->mime_type = $file_upload->getClientMimeType();
                    $file->save();
                }
                ez_log($request, 'p.file', $file->id, 'add');
                return redirect()->route('p.file.list');
            }
        }

        return view('panel.file.add', [
            'file' => $file,
            'errors' => $errors,
            'title' => 'Dodaj plik'
        ]);
    }

    private function rule()
    {
        return [
            'description' => 'required|max:256',
            'status' => 'required',
            'category' => 'required',
            'o_file' => 'required|file|mimes:' . File::ALLOWED_EXT
        ];
    }

}
