<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\Competition;
use App\Models\Country;
use App\Models\Category;
use App\Models\Organizer;
use App\Models\Region;
use App\Models\File;
use App\Models\Ctg;
use App\Models\Player;
use Illuminate\Support\Facades\Storage;

class CalendarController extends BackController
{

    public function listAction(Request $request)
    {
        if ($request->isMethod('post')){
            $arr = [];
            if ($request->q){$arr['q'] = $request->q;}
            if ($request->q_c){$arr['q_c'] = $request->q_c;}
            if ($request->q_r){$arr['q_r'] = $request->q_r;}
            if ($request->q_o){$arr['q_o'] = $request->q_o;}
            if ($request->q_z){$arr['q_z'] = $request->q_z;}
            if ($request->q_s){$arr['q_s'] = $request->q_s;}
            if ($request->q_t){$arr['q_t'] = implode('_', $request->q_t);}

            return redirect()->route('p.calendar.list', $arr);
        }
        if ($request->q || $request->q_c || $request->q_r || $request->q_o || $request->q_s || $request->q_t){

            $query = Competition::with('category', 'reg', 'res');
            if ($request->q){
                $q = ($request->q);
                $query->where(function($query) use($q){
                    $query->where('name', 'like', '%' . $q . '%')
                    ->orWhere('place', 'like', '%' . $q . '%');
                });
            }
            if ($request->q_c){
                $query->byCategory($request->q_c);
            }
            if ($request->q_s){
                $query->where('season', $request->q_s);
            }
            $calendar = $query->orderBy('date_start', 'asc')
            ->paginate(400);
        }else{
            $calendar = Competition::with('category', 'reg', 'res')
            ->where('season', date('Y'))
            ->orderBy('date_start', 'asc')
            ->paginate(400);
        }
        $min_year = intval(substr(Competition::min('date_start'), 0, 4));
        $max_year = intval(substr(Competition::max('date_start'), 0, 4));
        $seasons = [];
        foreach(range($min_year, $max_year) as $year){
            $seasons[$year] = $year;
        }
        
        #dd( [$max_year, $min_year] );
        return view('panel.calendar.list', [
            'calendar' => $calendar,
            'q' => $request->get('q', null),
            'q_c' => $request->get('q_c', null),
            'q_r' => $request->get('q_r', null),
            'q_o' => $request->get('q_o', null),
            'q_z' => $request->get('q_z', null),
            'q_s' => $request->get('q_s', date('Y')),
            'q_t' => ($request->q_t ? array_flip(explode('_', $request->q_t)) : 1),
            'category' => Category::getMap(),
            'seasons' => $seasons,
            'title' => 'Kalendarz'
        ]);
    }

    public function calendarAction(Request $request)
    {
        $calendar = Competition::with('category')
        ->orderBy('date_start', 'asc')
        ->paginate(400);

        return view('panel.calendar.calendar', [
            'calendar' => $calendar,
            'title' => 'Kalendarz'
        ]);
    }

    public function addAction(Request $request)
    {
        $errors = null;
        $competition = new Competition($request->all());

        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $competition->url = str_slug($competition->name);
                $competition->season = substr($competition->date_start, 0, 4);
                if ($request->lat){
                    $competition->lat = str_replace(',', '.', $request->lat);
                }
                if ($request->lng){
                    $competition->lng = str_replace(',', '.', $request->lng);
                }
                $competition->save();
                $competition->url = str_slug($competition->id.' '.$competition->name);
                $competition->save();
                $ctg_select = Ctg::whereIn('id', $request->get('ctg_id', []))->get();
                if ($ctg_select->count()){
                    foreach ($ctg_select as $row){
                        $competition->ctgs()->attach($row->id);
                    }
                }
                ez_log($request, 'p.competition', $competition->id, 'add');
                ez_clean_cache('calendar');
                return redirect()->route('p.calendar.edit', ['id' => $competition->id]);
            }
        }

        return view('panel.calendar.add', [
            'competition' => $competition,
            'category' => Category::getMap(true),
            'ctg' => Ctg::getMap(),
            'errors' => $errors,
            'title' => 'Dodaj wyścig'
        ]);
    }

    public function editAction(Request $request)
    {
        $errors = null;
        $competition = Competition::find($request->id);
        $competition_info = Competition::with('reg', 'res', 'ctgs', 'players')
        ->find($request->id);
        if (!$competition){
            abort(404);
        }
        #dd($competition_info);
        $ctg_curr = [];
        if ($competition_info->ctgs->count()){
            foreach ($competition_info->ctgs as $row){
                $ctg_curr[] = $row->id;
            }
        }

        if ($request->delete){
            $competition->delete();
            ez_log($request, 'p.competition', $competition->id, 'delete');
            ez_clean_cache('calendar');
            return redirect()->route('p.calendar');
        }

        if ($request->geolocation){
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($competition->place);
            $competition->geo = @file_get_contents($url);
            $gps = json_decode($competition->geo);
            $competition->lat = ($gps->results ? $gps->results[0]->geometry->location->lat : 1);
            $competition->lng = ($gps->results ? $gps->results[0]->geometry->location->lng : 1);
            $competition->save();
            ez_log($request, 'p.competition', $competition->id, 'edit_update_geo');
            ez_clean_cache('calendar');
            ez_clean_single('calendar', $competition->id);
            return redirect()->route('p.calendar.edit', ['id' => $competition->id]);
        }

        if ($request->isMethod('post') && \Entrust::can('p.calendar')){
            $validator = \Validator::make($request->all(), $this->rule());
            $competition->fill($request->all());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $competition->ctgs()->detach();
                $ctg_select = Ctg::whereIn('id', $request->get('ctg_id', []))->get();
                if ($ctg_select->count()){
                    foreach ($ctg_select as $row){
                        $competition->ctgs()->attach($row->id);
                    }
                }
                $competition->season = substr($competition->date_start, 0, 4);
                if ($request->lat){
                    $competition->lat = str_replace(',', '.', $request->lat);
                }
                if ($request->lng){
                    $competition->lng = str_replace(',', '.', $request->lng);
                }
                $competition->save();
                ez_log($request, 'p.competition', $competition->id, 'edit');
                ez_clean_cache('calendar');
                ez_clean_single('calendar', $competition->id);
                return redirect()->route('p.calendar.edit', ['id' => $competition->id]);
            }
        }

        $view = [
            'competition' => $competition,
            'competition_info' => $competition_info,
            'category' => Category::getMap(true),
            'ctg' => Ctg::getMap(),
            'ctg_curr' => $ctg_curr,
            'title' => 'Edytuj wyścig'
        ];
        if (count($errors)){
            $view['errors'] = $errors;
        }
        return view('panel.calendar.edit', $view);
    }
    
    public function playersAction(Request $request)
    {
        $competition = Competition::with(['players' => function($query){
            $query->orderBy('created_at', 'asc');
        }])
        ->find($request->id);
        if (!$competition){
            abort(404);
        }
        
        
        
        $contents = view('prints.doc.competition.players', [
            'competition' => $competition
        ]);
        #return $contents;
        $file_name = 'result_' . date('YmdHis') . '.doc';
        Storage::put('results/' . $file_name, $this->ch($contents));

        return \Response::download(storage_path('app/results/') . $file_name);
    }

    public function editRelationAction(Request $request)
    {
        $competition = Competition::find($request->id);

        if (!$competition){
            abort(404);
        }

        if ($request->unlink){
            $file = File::find($request->file_id);
            if ($file){
                \Illuminate\Support\Facades\File::delete(public_path($file->path) . $file->name);
                $file->delete();
                ez_log($request, 'p.file', $file->id, 'delete');
                ez_log($request, 'p.competition', $competition->id, 'edit_file_delete_' . $file->id);
                ez_clean_cache('calendar');
                ez_clean_single('calendar', $competition->id);
            }
            return redirect()->route('p.calendar.edit', ['id' => $competition->id]);
        }

        if ($request->isMethod('post')){
            if ($request->is_reg_form){
                $validator = \Validator::make($request->all(), $this->rule_file_reg());
                if ($validator->fails()){
                    $request->session()->flash('errors', $validator->messages());
                    #dd($request->session()->all());
                }else{
                    $file = new File();
                    $file->description = $request->desc;
                    $file->user_id = \Auth::user()->id;
                    $file->category = File::CATEGORY_REGULATION;
                    $file->save();
                    // File uploading
                    if ($file_upload = $request->file('reg_file')){
                        $file_name = $file->id . '_' . str_limit(str_slug($file->description . $competition->name, '_'), 33, '') . '.' . strtolower($file_upload->getClientOriginalExtension());
                        $file->path = 'pliki/' . date('Y') . '/';
                        $file_upload->move(ez_storage($file->path), $file_name);
                        $file->name = $file_name;
                        $file->name_original = $file_upload->getClientOriginalName();
                        $file->ext = strtolower($file_upload->getClientOriginalExtension());
                        $file->mime_type = $file_upload->getClientMimeType();
                        $file->save();
                    }
                    $competition->files()->attach($file->id, [
                        'created_at' => new \DateTime(),
                        'updated_at' => new \DateTime(),
                    ]);
                    ez_log($request, 'p.file', $file->id, 'add');
                    ez_log($request, 'p.competition', $competition->id, 'edit_file_add_' . $file->id);
                    ez_clean_cache('calendar');
                    ez_clean_single('calendar', $competition->id);
                }
            }else if ($request->is_res_form){
                $validator = \Validator::make($request->all(), $this->rule_file_res());
                if ($validator->fails()){
                    $request->session()->flash('errors', $validator->messages());
                }else{
                    $file = new File();
                    $file->description = $request->desc;
                    $file->user_id = \Auth::user()->id;
                    $file->category = File::CATEGORY_RESULT;
                    $file->save();
                    // File uploading
                    if ($file_upload = $request->file('res_file')){
                        $file_name = $file->id . '_' . str_limit(str_slug($file->description . $competition->name, '_'), 33, '') . '.' . strtolower($file_upload->getClientOriginalExtension());
                        $file->path = 'pliki/' . date('Y') . '/';
                        $file_upload->move(ez_storage($file->path), $file_name);
                        $file->name = $file_name;
                        $file->name_original = $file_upload->getClientOriginalName();
                        $file->ext = strtolower($file_upload->getClientOriginalExtension());
                        $file->mime_type = $file_upload->getClientMimeType();
                        $file->save();
                    }
                    $competition->files()->attach($file->id, [
                        'created_at' => new \DateTime(),
                        'updated_at' => new \DateTime(),
                    ]);
                    ez_log($request, 'p.file', $file->id, 'add');
                    ez_log($request, 'p.competition', $competition->id, 'edit_file_add_' . $file->id);
                    ez_clean_cache('calendar');
                    ez_clean_single('calendar', $competition->id);
                }
            }
        }
        return redirect()->route('p.calendar.edit', ['id' => $competition->id]);
    }

    private function rule()
    {
        return [
            'name' => 'required|max:256',
            'date_start' => 'required',
            'place' => 'required|max:256',
            'category_id' => 'required',
        ];
    }

    private function rule_file_reg()
    {
        return [
            'desc' => 'required|max:256',
            'reg_file' => 'required|file|mimes:' . config('ez.validator.file_upload_in_calendar'),
        ];
    }

    private function rule_file_res()
    {
        return [
            'desc' => 'required|max:256',
            'res_file' => 'required|file|mimes:' . config('ez.validator.file_upload_in_calendar')
        ];
    }

    public function ch($string)
	{
		return @iconv('utf-8', 'windows-1250', $string);
	}
}
