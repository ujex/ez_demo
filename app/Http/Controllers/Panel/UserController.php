<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
//use Illuminate\Support\Facades\Mail;
//use App\Mail\NewPassword;

class UserController extends BackController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listAction(Request $request)
    {
        if ($request->q){
            $q = str_slug($request->q);
            $users = User::with('sociales')->where('login', 'like', '%' . $q . '%')
            ->orWhere('name', 'like', '%' . $q . '%')
            ->orWhere('surname', 'like', '%' . $q . '%')
            ->orWhere('email', 'like', '%' . $q . '%')
            ->orderBy('created_at', 'desc')
            ->paginate(config('ez.pagination.global'));
        }else{
            $users = User::with('sociales')->orderBy('created_at', 'desc')
            ->paginate(config('ez.pagination.global'));
        }

        return view('panel.user.list', [
            'users' => $users,
            'title' => 'Lista użytkowników'
        ]);
    }

    public function addAction(Request $request)
    {
        $errors = null;
        $user = new User();

        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $user->name = $request->firstname;
                $user->surname = $request->surname;
                $user->email = $request->email;
                $user->status = $request->status;
                $user->password = \Illuminate\Support\Facades\Hash::make($request->pass);
                $user->login = substr(str_slug($request->firstname), 0, 1) . str_slug($request->surname);
                $user->type = 'A';
                $user->save();
                ez_log($request, 'p.user', $user->id, 'add');
                return redirect()->route('p.user.edit', ['id' => $user->id]);
            }
        }

        return view('panel.user.add', [
            'user' => $user,
            'errors' => $errors,
            'title' => 'Dodaj użytkownika'
        ]);
    }

    public function editAction(Request $request)
    {
        $errors = null;
        $user = User::find($request->id);

        if ($request->isMethod('post')){
            if ($request->is_data_form){
                $validator = \Validator::make($request->all(), $this->rule());
                if ($validator->fails()){
                    $errors = $validator->messages();
                }else{
                    $user->name = $request->firstname;
                    $user->surname = $request->surname;
                    $user->email = $request->email;
                    $user->status = $request->status;
                    $user->save();
                    ez_log($request, 'p.user', $user->id, 'edit');
                    return redirect()->route('p.user.edit', ['id' => $user->id]);
                }
            }
            if ($request->is_auth_form){
                $validator = \Validator::make($request->all(), ['pass' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/']); //required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/
                if ($validator->fails()){
                    $errors = $validator->messages();
                }else{
//                    Mail::to($user->email)->bcc(config('ez.log_email'))
//                    ->send(new NewPassword($user->email, $request->pass));
                    $user->password = \Illuminate\Support\Facades\Hash::make($request->pass);
                    $user->save();
                    ez_log($request, 'p.user', $user->id, 'edit_change_password');
                    return redirect()->route('p.user.edit', ['id' => $user->id]);
                }
            }
        }

        // Role
        if ($request->role){
            $role = Role::find($request->role);
            if ($request->option){
                $user->attachRole($role);
                ez_log($request, 'p.role', $role->id, 'attachRole');
            }else{
                $user->detachRole($role);
                ez_log($request, 'p.role', $role->id, 'detachRole');
            }
            return redirect()->route('p.user.edit', ['id' => $user->id]);
        }
        $role_set = Role::select(['roles.*'])
        ->join('role_user', 'roles.id', '=', 'role_user.role_id')
        ->where('role_user.user_id', '=', $user->id)
        ->get()->toArray();
        $role_tmp = Role::get()->toArray();
        $role_not_set_tmp = array_diff(array_map('serialize', $role_tmp), array_map('serialize', $role_set));
        $role_not_set = [];
        if ($role_not_set_tmp){
            foreach ($role_not_set_tmp as $key){
                $role_not_set[] = unserialize($key);
            }
        }

        return view('panel.user.edit', [
            'user' => $user,
            'role_set' => $role_set,
            'role_not_set' => $role_not_set,
            'errors' => $errors,
            'title' => 'Edycja użytkownika'
        ]);
    }

    public function roleAction()
    {
        $roles = Role::orderBy('created_at', 'asc')
        ->paginate(200);

        return view('panel.user.role', [
            'roles' => $roles,
            'title' => 'Lista grup'
        ]);
    }

    public function roleEditAction(Request $request)
    {
        $role = Role::find($request->id);
        if ($request->perm){
            $permission = Permission::find($request->perm);
            if ($request->option){
                $role->attachPermission($permission);
                ez_log($request, 'p.permission', $permission->id, 'attachPermission');
            }else{
                $role->detachPermission($permission);
                ez_log($request, 'p.permission', $permission->id, 'detachPermission');
            }
            return redirect()->route('p.user.role.edit', ['id' => $role->id]);
        }
        $perm_set = Permission::select(['permissions.*'])
        ->join('permission_role', 'permissions.id', '=', 'permission_role.permission_id')
        ->where('permission_role.role_id', '=', $role->id)
        ->get()->toArray();

        $perm_tmp = Permission::get()->toArray();
        $perm_not_set_tmp = array_diff(array_map('serialize', $perm_tmp), array_map('serialize', $perm_set));
        $perm_not_set = [];
        if ($perm_not_set_tmp){
            foreach ($perm_not_set_tmp as $key){
                $perm_not_set[] = unserialize($key);
            }
        }

        #dd([$perm_not_set,$perm_set]);
        return view('panel.user.role-edit', [
            'role' => $role,
            'perm_set' => $perm_set,
            'perm_not_set' => $perm_not_set,
            'title' => 'Przypisz moduły do grupy ' . $role->name
        ]);
    }

    public function permissionAction()
    {
        $permissions = Permission::orderBy('created_at', 'desc')
        ->paginate(200);

        return view('panel.user.permission', [
            'permissions' => $permissions,
            'perm_model' => new Permission(),
            'title' => 'Lista modułów'
        ]);
    }

    public function permissionAddAction(Request $request)
    {
        $permission = new Permission();
        if ($request->isMethod('post')){
            $permission->name = $request->name;
            $permission->display_name = $request->display_name;
            $permission->description = $request->description;
            $validator = \Validator::make($request->all(), [
                'name' => 'required|max:250',
                'display_name' => 'required|max:250'
            ]);
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $permission->save();
                ez_log($request, 'p.permission', $permission->id, 'add');

                $super_admin = Role::where('name', '=', 'super-admin')->first();
                $super_admin->attachPermission($permission);
            }
        }

        return redirect()->route('p.user.permission');
    }

    private function rule()
    {
        return [
            'firstname' => 'required|max:250',
            'surname' => 'required|max:128',
            'email' => 'email',
        ];
    }

}
