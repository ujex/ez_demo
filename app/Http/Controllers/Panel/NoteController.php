<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\Note;


class NoteController extends BackController
{

    public function listAction(Request $request)
    {
        $query = Note::orderBy('id', 'asc');

        if ($request->q){
            $q = ($request->q);
            $query->where(function($query) use($q){
                $query->where('title', 'like', '%' . $q . '%')
                ->orWhere('code', 'like', '%' . $q . '%')
                ->orWhere('content', 'like', '%' . $q . '%')
                ->orWhere('type', 'like', '%' . $q . '%');
            });
        }

        $notes = $query->paginate(config('ez.pagination.global'));
        return view('panel.note.list', [
            'notes' => $notes,
            'q' => $request->get('q', null),
            'title' => 'Konfiguracja'
        ]);
    }

    public function editAction(Request $request)
    {
        $errors = null;
        
        $note = Note::find($request->id);
        if (!$note){
            abort(404);
        }

        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule_edit());
            $note->fill($request->all());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $note->save();
                ez_log($request, 'p.note', $note->id, 'edit');
                ez_clean_cache('all');
                return redirect()->route($request->get('redirect', 'p.note.list'));
            }
        }

        return view('panel.note.edit', [
            'note' => $note,
            'errors' => $errors,
            'title' => 'Edytuj wpis',
        ]);
    }
    
    private function rule_edit()
    {
        return [
            'code' => 'required|max:128',
        ];
    }
}
