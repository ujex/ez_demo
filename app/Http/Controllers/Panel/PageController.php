<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Img;
use App\Models\Category;
use Image;

class PageController extends BackController
{

    public function listAction(Request $request)
    {
        $query = Page::select(['pages.*']);
        if ($request->q){
            $q = ($request->q);
            $query->where(function($query) use($q){
                $query->where('title', 'like', '%' . $q . '%')
                ->orWhere('url', 'like', '%' . $q . '%')
                ->orWhere('content', 'like', '%' . $q . '%');
            });
        }
        if (!\Entrust::can('p.page')){
            $query->whereIn('permission', \Auth::user()->getPermissions());
        }
        $query->orderBy('title', 'asc');
        $pages = $query->paginate(config('ez.pagination.global'));
        return view('panel.page.list', [
            'pages' => $pages,
            'q' => $request->get('q', null),
            'title' => 'Lista stron statycznych'
        ]);
    }

    public function addAction(Request $request)
    {
        $errors = null;
        $page = new Page($request->all());

        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $page->user_id = \Auth::user()->id;
                $page->url = str_slug($page->title);
                $page->save();
                ez_log($request, 'p.page', $page->id, 'add');
                return redirect()->route('p.page.list');
            }
        }

        return view('panel.page.add', [
            'page' => $page,
            'errors' => $errors,
            'title' => 'Dodaj stronę'
        ]);
    }

    public function editAction(Request $request)
    {
        $errors = null;
        if (!\Entrust::can('p.page')){
            $page = Page::whereIn('permission', \Auth::user()->getPermissions())->find($request->id);
        }else{
            $page = Page::find($request->id);
        }
        $page_info = Page::with('user', 'img')
        ->find($request->id);
        if (!$page){
            return redirect()->route('p.not-allowed');
        }

        if ($request->unlink){
            $file = Img::find($request->image_id);
            if ($file){
                \Illuminate\Support\Facades\File::delete(public_path('img/thumbnail/original/') . $file->name);
                \Illuminate\Support\Facades\File::delete(public_path('img/thumbnail/800/') . $file->name);
                \Illuminate\Support\Facades\File::delete(public_path('img/thumbnail/830/') . $file->name);
                \Illuminate\Support\Facades\File::delete(public_path('img/thumbnail/300/') . $file->name);
                \Illuminate\Support\Facades\File::delete(public_path('img/thumbnail/270/') . $file->name);
                \Illuminate\Support\Facades\File::delete(public_path('img/thumbnail/100/') . $file->name);
                $file->delete();
                $page->image_id = null;
                $page->save();
                ez_log($request, 'p.page', $page->id, 'edit_img_delete_' . $file->id);
            }
            return redirect()->route('p.post.edit', ['id' => $page->id]);
        }

        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule_edit());
            $page->fill($request->all());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                // File uploading
                if ($file = $request->file('img_file')){
                    $file_name = (new \DateTime('now'))->format('Ymd_His') . '.' . strtolower($request->file('img_file')->getClientOriginalExtension());
                    $request->file('img_file')->move(public_path('img/thumbnail/original'), $file_name);
                    // Creating thumbnails
                    Image::make(public_path('img/thumbnail/original/') . $file_name)->fit(800, 600)->save(public_path('img/thumbnail/800/') . $file_name);
                    Image::make(public_path('img/thumbnail/original/') . $file_name)->fit(830, 435)->save(public_path('img/thumbnail/830/') . $file_name);
                    Image::make(public_path('img/thumbnail/original/') . $file_name)->fit(300, 225)->save(public_path('img/thumbnail/300/') . $file_name);
                    Image::make(public_path('img/thumbnail/original/') . $file_name)->fit(270, 180)->save(public_path('img/thumbnail/270/') . $file_name);
                    Image::make(public_path('img/thumbnail/original/') . $file_name)->fit(100, 75)->save(public_path('img/thumbnail/100/') . $file_name);
                    $image = new Img();
                    $image->name = $file_name;
                    $image->name_original = $file->getClientOriginalName();
                    $image->type = Img::TYPE_THUMB;
                    $image->user_id = \Auth::user()->id;
                    $image->save();
                    $page->image_id = $image->id;
                }
                $page->save();
                ez_log($request, 'p.page', $page->id, 'edit');
                \Cache::forget('main_menu');
                return redirect()->route($request->get('redirect', 'p.page.list'));
            }
        }

        $disabled = [
            'title' => [],
        ];
        if ($page->menu_id && !\Entrust::can('p.menu')){
            $disabled['title'] = ['readonly' => 'readonly'];
        }

        return view('panel.page.edit', [
            'page' => $page,
            'page_info' => $page_info,
            'errors' => $errors,
            'redirect' => $request->get('redirect', 'p.page.list'),
            'title' => 'Edytuj stronę',
            'disabled' => $disabled,
        ]);
    }

    private function rule()
    {
        return [
            'title' => 'required|max:256',
            'content' => 'required',
            'img_file' => 'image' // 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    private function rule_edit()
    {
        return [
            'title' => 'required|max:256',
            'content' => 'required',
            'img_file' => 'image' // 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

}
