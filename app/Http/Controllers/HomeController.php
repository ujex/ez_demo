<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Jobs\SendVerificationEmail;
use App\Models\Competition;
use App\Models\File;

class HomeController extends FrontController
{

    public function indexAction(Request $request)
    {

//        $ch = curl_init(); 
//        curl_setopt($ch, CURLOPT_URL, 'http://bookle.gp/api/manage-order/confirm'); 
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, [
//            'x-security-token: ' . md5('&gw8a7sdaf'.date('Y-m-d')),
//        ]);
//        curl_setopt($ch, CURLOPT_POSTFIELDS,
//            "order_id=262");
//        $output = curl_exec($ch); 
//        curl_close($ch);    
//        
//   
//        echo '<pre>';
//        print_r($output);
//        die();
        $comp = Competition::with('category')
        ->where('date_start', '<=', date('Y-m-d'))
        ->orderBy('date_start', 'desc')
        ->limit(20)->get();


        return view('home.index', [
            'comp' => $comp,
            'title' => 'Obsługa zawodów sportowych, Pomiar czasu'
        ]);
    }
    
    public function redirAction(Request $request)
    {
        $p = $request->getPathInfo();
        if($p == '/inne/oferta.html'){
            return \Redirect::to(url('/pl/1,offer.html'), 301);
        }
        if($p == '/inne/kontakt.html'){
            return \Redirect::to(url('/pl/2,contact.html'), 301);
        }
        if($p == '/wyniki/'){
            return \Redirect::to(url('/pl/results'), 301);
        }
        if($p == '/wyniki/0,mtb/' || $p == '/wyniki/0,mtb'){
            return \Redirect::to(url('/pl/results/mtb'), 301);
        }
        if($p == '/wyniki/0,szosa/' || $p == '/wyniki/0,szosa'){
            return \Redirect::to(url('/pl/results/road'), 301);
        }
        if($p == '/wyniki/0,przelaj/' || $p == '/wyniki/0,przelaj'){
            return \Redirect::to(url('/pl/results/cyclo-cross'), 301);
        }
        
        if(strpos($p, '.html')){
            return \Redirect::to(url('/pl' . substr($p, 7)), 301);
        }
        return \Redirect::to(url('/'), 301);
    }

    public function testAction()
    {
        die();
        $db = \DB::connection('mysql2');
        $races = $db->select("SELECT * FROM race r "

        . " WHERE `status` > 0");
        $i = 1;
        $ctg = [
            '1' => 3,
            '2' => 2,
            '3' => 5,
        ];
        foreach($races as $race){
            $season = substr($race->date, 0, 4);
            $cn = new Competition();
            $cn->name = $race->name;
            $cn->url = $race->url;
            $cn->code = $race->code;
            $cn->info = $race->info;
            $cn->place = $race->city;
            $cn->organiser = $race->organiser;
            $cn->date_start = $race->date;
            $cn->lat = $race->gps_lat;
            $cn->lng = $race->gps_lng;
            $cn->season = $season;
            $cn->category_id = $ctg[$race->category_id];
            $cn->save();
            
            $files = $db->select("SELECT * FROM `file` WHERE `race_id` = " . $race->race_id);
            foreach($files as $file){
                $p = explode('.', $file->path);
                
                $fn = new File();
                $fn->description = $file->title;
                $fn->user_id = \Auth::user()->id;
                $fn->category = ($file->ftype==2?(File::CATEGORY_REGULATION):(File::CATEGORY_RESULT));
                $fn->save();
                $content = @file_get_contents('http://e-zawody.pl/public'.$file->path);
                $file_name = $fn->id . '_' . str_limit(str_slug($fn->description . $cn->name, '_'), 37, '') . '.' . strtolower($p[1]);
                $fn->path = 'pliki/' . $season . '/';
                $fn->name = $file_name;
                $fn->name_original = $file_name;
                $fn->ext = strtolower($p[1]);
                $fn->save();

                $cn->files()->attach($fn->id, [
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime(),
                ]);
                    
                \Storage::disk('public')->put($fn->path . $file_name, $content);
            }

            
            $i++;
//            if($i > 45){
//                die();
//            }
            echo $race->name;
        }
        
        
        dd($races);
        $user = User::find(14);
        dispatch(new SendVerificationEmail($user));

        $t = 't';
        $t = \Entrust::hasRole('rol');

        return view('home.index', [
            't' => var_export([$t], true)
        ]);
    }

}
