<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Competition;
use App\Models\Rally;
use App\Models\Result;

class ResultController extends \App\Http\Controllers\Controller
{

    public function set(Request $request)
    {
        $res_nag = json_decode($request->res_nag);
        $res_pos = json_decode($request->res_pos);
        if(!isset($res_nag->race_id)){
            throw new \Exception('Param err');
        }
        $race_id = addslashes($res_nag->race_id);
        $token = substr(md5($race_id), 7, 13);
        if($token != $request->token){
            return response()->json([
                'success' => false,
                'msg' => "Access denied",
                'count' => 0,
                'data' => null
            ]);
        }
        $competition = Competition::find($race_id);
        
        $rally = Rally::where('competition_id', $competition->id)
            ->where('slug', str_slug($res_nag->name))
            ->first();
        if(!$rally){
            $rally = new Rally();
            $rally->competition_id = $competition->id;
            $rally->name = $res_nag->name;
            $rally->slug = str_slug($res_nag->name);
            $rally->save();
        }
        $rally->length = $res_nag->length;
        $rally->speed = $res_nag->speed;
        $rally->info = $res_nag->info;
        $rally->status = $res_nag->status;
        $rally->save();
        
        Result::where('rally_id', $rally->id)->delete();
        
        foreach($res_pos as $p){
            $result = new Result();
            $result->lp = $p->lp;
            $result->pos = $p->pos;
            $result->bib = $p->bib;
            $result->surname = $p->surname;
            $result->name = $p->name;
            $result->code = $p->code;
            $result->team = $p->team;
            $result->birth_date = $p->birth_date;
            $result->result = $p->result;
            $result->diff = $p->diff;
            $result->info = $p->info;
            $result->rally_id = $rally->id;
            $result->country_id = 144;
            $result->save();
        }
        return response()->json([
            'success' => true,
            'msg' => "Results updated successful",
            'count' => count($res_pos),
            'data' => $res_nag
        ]);
    }

    public function delete(Request $res_nag)
    {

        return response()->json($json);
    }

}
