<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Player;


class PlayerController extends \App\Http\Controllers\Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function searchAction(Request $request)
    {
        $query = Player::with('team')
            ->where('name_clean', 'like', "%".str_slug($request->term)."%")
            ->orderBy('name_clean', 'asc')
        ;
        if($request->type != 'ALL'){
            $query->whereIn('type', explode('|', $request->type));
        }
        $player = $query->get();
        $json = [];
        if($player){
            foreach($player as $p){
                $json[] = [
                    'label' => $p->surname . ', ' . $p->name . ' | ' . $p->birth_date . '' . ' | ' . $p->team->name,
                    'id' => $p->id
                ];
            }
        }
        return response()->json($json);
    }
    
}