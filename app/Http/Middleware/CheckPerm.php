<?php

namespace App\Http\Middleware;

use Closure;

class CheckPerm
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $arg_num = func_num_args();
        $arg_list = func_get_args();
        $allow = false;

        /**
         * @todo przerobić na wbudowaną metodę
         */
        for ($i = 2; $i < $arg_num; $i++){
            if (\Entrust::can($arg_list[$i])){
                $allow = true;
            }
        }
        
        if (!$allow || !$request->user()->status){
            return redirect()->route('p.not-allowed');
        }
        return $next($request);
    }

}
