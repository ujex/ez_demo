<?php

namespace App\Http\Middleware;

use Closure;

class Localize
{

    public function handle($request, Closure $next)
    {
        $locale = $request->segment(1);
        if (in_array($locale, config('app.locale_allowed'))){
            \App::setLocale($locale);
        }
        $locale = \App::getLocale();
        $request->session()->put('locale', $locale);
        \Carbon\Carbon::setLocale($locale);
        view()->composer('*', function ($view) use ($locale){
            $view->with([
                'loc' => $locale,
            ]);
        });
        return $next($request);
    }

}
