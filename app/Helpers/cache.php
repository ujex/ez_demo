<?php

/**
 * Clear home and list cache for section
 * 
 * @param string $section
 */
function ez_clean_cache($section)
{
    \Cache::flush();
    return true;
    $regions = App\Models\Region::get();
    $category = App\Models\Category::get();
    foreach ($regions as $region){
        foreach ($category as $ctg){
            \Cache::forget($section . '_home_' . $ctg . $region->id);
            \Cache::forget($section . '_list_' . $ctg . $region->id);
            \Cache::forget($section . '_box_' . $ctg . $region->id);
        }
    }
}

function ez_clean_single($section, $id)
{
    \Cache::flush();
    return true;
    \Cache::forget($section . '_' . $id);
}
