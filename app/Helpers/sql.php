<?php

/**
 * Reset auto increment
 *
 * @param string $table
 */
function ez_reset_ai($table)
{
    $max = \DB::table($table)->max('id');
    \Db::select('alter table `' . $table . '` auto_increment = ' . ($max + 1));
}

/**
 * Log event
 *
 * @param \Illuminate\Http\Request $request
 * @param string $symbol
 * @param int $foreign_id
 * @param string $message
 */
function ez_log(\Illuminate\Http\Request $request, $symbol, $foreign_id, $message = null)
{
    $log = new \App\Models\Log();
    $log->symbol = $symbol;
    $log->foreign_id = $foreign_id;
    $log->message = $message;
    $log->path = $request->getHost() . $request->getPathInfo();
    $log->post = json_encode($request->all());
    $log->user_agent = $request->header('User-Agent');
    $log->ip = $request->getClientIp();
    $log->user_id = isset(\Auth::user()->id) ? \Auth::user()->id : 0;

    $log->save();
}
