<?php

/**
 * Make string uppercase
 *
 * @param string $expression
 * @return string converted string
 */
function ez_uc($expression)
{
    return mb_convert_case($expression, MB_CASE_UPPER, "utf-8");
}

/**
 * Make string lowercase
 *
 * @param string $expression
 * @return string converted string
 */
function ez_lc($expression)
{
    return mb_convert_case($expression, MB_CASE_LOWER, "utf-8");
}

/**
 * Check odd in queue
 *
 * @param int $iterator
 * @param mixed $pos return this value if iterator is odd
 * @param mixed $neg return this value if iterator is not odd
 * @return mixed pos or neg
 */
function ez_odd($iterator, $pos, $neg = '')
{
    return ($iterator % 2) ? $pos : $neg;
}

/**
 * Price format
 *
 * @param float $price
 * @param string $dot point separator
 * @return string price
 */
function ez_price($price, $dot = '.')
{
    return number_format(rount($price, 2), 2, $dot, ' ');
}

/**
 * Convert date to human readable
 * 
 * @param string $date
 * @return string formated $date
 */
function ez_date_long($date)
{
    if (config('app.locale') != 'pl'){
        return \Carbon\Carbon::parse($date)->formatLocalized('%d %B %Y');
    }
    $months = ['01' => 'Stycznia', '02' => 'Lutego', '03' => 'Marca', '04' => 'Kwietnia',
        '05' => 'Maja', '06' => 'Czerwca', '07' => 'Lipca', '08' => 'Sierpnia', '09' => 'Września',
        '10' => 'Października', '11' => 'Listopada', '12' => 'Grudnia'];
    $date_exploded = explode('-', $date);
    return $date_exploded[2] . ' ' . $months[$date_exploded[1]] . ' ' . $date_exploded[0];
}

/**
 * Get <img> with country flag
 * 
 * @param string $country
 * @param int $size [1=16, 2=24, 3=32, 4=48]
 * @return type
 */
function ez_flag($country, $size = 16)
{
    $size_array = [1 => 16, 2 => 24, 3 => 32, 4 => 48];
    if ($size < 16){
        $size = $size_array[$size];
    }
    return '<img src="' . asset('flags/' . $size . '/' . $country . '.png') . '" alt="' . $country . '">';
}

/**
 * Get span label to obect's status
 * 
 * @param int $status
 * @return string
 */
function ez_status($status)
{
    switch ($status){
        case 0:
            return '<span class="label label-danger">Nieaktywny</span>';
        case 1:
            return '<span class="label label-success">Aktywny</span>';
        default:
            return '<span class="label label-default">Inny</span>';
    }
}

/**
 * Check if is error on input key
 * 
 * @since 2.0
 * @param Illuminate\Support\MessageBag $errors
 * @param string $key input name value
 * @return string
 */
function ez_err(Illuminate\Support\MessageBag $errors, $key)
{
    if (!$errors->count()){
        return '';
    }
    if ($errors->has($key)){
        return ' text-red ';
    }
    return '';
}

function ez_asset($url, $version = false, $min = false)
{
    if($version){
        $url .= '?v='.config('ez.asset_version');
    }
    if(config('app.env') == 'local' && $min){
        $url = str_replace(".min.", ".", $url);
        $url .= time();
    }
    return asset('tpl/'.config('ez.tpl').'/' . $url);
}

function ez_storage($file)
{
    return storage_path('app/public/' . $file);
}

function ez_phone(string $phone){
    $phone = trim($phone);
    $phone = str_replace([' ','-','+48','+','(',')','.'], ['','','','','','',''], $phone);
    $phone = str_replace(['+48'], [''], $phone);
    return $phone;
}