<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Backup::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // job worker - nie wolno na linuxpl.com
		#$schedule->command('queue:work --tries=3')->cron('* * * * * *')->withoutOverlapping()->appendOutputTo(storage_path('logs/cron.log'));
        
        // backup - nie działa na linuxpl.com
        #$schedule->command('backup')->daily()->appendOutputTo(storage_path('logs/cron.log'));
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
        $this->load(__DIR__.'/Commands');
    }
}
