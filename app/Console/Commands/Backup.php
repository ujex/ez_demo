<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Backup extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pseudo-code of a command that performs a daily backup of an database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        shell_exec('mysqldump --single-transaction --skip-lock-tables -P '.env('DB_PORT').' -h '.env('DB_HOST').' -u '.env('DB_USERNAME').' -p'.env('DB_PASSWORD').' '.env('DB_DATABASE').' | gzip > ' . storage_path('app/db-backup/'.date('Ymd_His').'.sql.gz'));
        $filesCollection = collect( \Storage::files('db-backup') );
        if ($filesCollection->count() > 31) {
            \Storage::delete($filesCollection->sort()->first());
        }
    }

}
