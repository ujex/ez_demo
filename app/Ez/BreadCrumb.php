<?php

namespace App\Ez;

class BreadCrumb
{

    private static $crumbs = [];

    /**
     * Add element to breadcrumbs
     * 
     * @param string $name
     * @param string $path
     */
    public static function add($name, $path = null)
    {
        if (!self::$crumbs){
            self::setHome();
        }
        self::addCrumb($name, $path);
    }

    /**
     * Get breadcrumbs
     *  
     * @return array
     */
    public static function get()
    {
        return self::$crumbs;
    }

    /**
     * Set basic home element
     */
    public static function setHome()
    {
        self::addCrumb('Start', '/');
    }

    private static function addCrumb($name, $path = null)
    {
        self::$crumbs[] = [
            'path' => $path,
            'name' => $name
        ];
    }

}
