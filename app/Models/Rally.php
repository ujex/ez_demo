<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rally extends Model
{

    public $timestamps = true;
    protected $table = 'rally';
    protected $fillable = ['name', 'slug', 'length', 'speed', 'info', 'status', 'competition_id'];
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The rally that belong to the competition.
     */
    public function result()
    {
        return $this->hasMany('App\Models\Result');
    }

}
