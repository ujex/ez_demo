<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @mixin \Eloquent
 */
class Category extends Model
{

    public $timestamps = false;
    protected $table = 'category';
    protected $fillable = ['name', 'description', 'url'];

    public static function getMap($only_real = false)
    {
        $region = self::orderBy('id', 'asc')->get();
        $map = [];
        foreach ($region as $c){
            if(!$only_real){
                $map[$c->id] = $c->name;
            }else{
                if($c->url){
                    $map[$c->id] = $c->name;
                }
            }
        }
        return $map;
    }

    /**
     * Get the posts for the category.
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

}
