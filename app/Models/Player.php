<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Player extends Model
{

    public $timestamps = true;
    protected $table = 'players';
    protected $fillable = ['name', 'surname', 'name_clean', 'birth_date', 'sex',
        'uci_id', 'pzkol_id', 'uci_category', 'type', 'type_desc', 'license', 'city', 'phone', 'email', 'medical',
        'rank_score', 'rank_position', 'status', 'com_info', 'foregin_id', 'foregin_team_id', 'region_id', 'country_id',
        'user_id', 'team_id','team', 'ez_id'
    ];
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Year of birth
     * 
     * @return string
     */
    public function birth_year()
    {
        return $this->birth_date->format('Y');
    }

    public function getName()
    {
        return $this->surname . ' ' . $this->name;
    }

    public function getPhone()
    {
        if(!$this->phone){
            return $this->phone;
        }
        if (in_array(substr($this->phone, 0, 1), ['+', '0'])){
            return $this->phone;
        }elseif (substr($this->phone, 0, 2) == '48'){
            return '+' . $this->phone;
        }else{
            return '+48' . $this->phone;
        }
        return ez_flag('pl') . ' ' . $this->phone;
    }

    /**
     * Formated UCI ID
     * 
     * @return string
     */
    public function uciId()
    {
        return chunk_split($this->uci_id, 3, ' ');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    
    /**
     * The teams that belong to the player.
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }

    /**
     * Get the country that owns the player.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the region that owns the player.
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function save(array $options = [])
    {
        if ($this->name && $this->surname){
            $this->name_clean = str_slug($this->surname . ' ' . $this->name);
        }

        parent::save();
    }
    

    /**
     * Male
     */
    const MALE = 'M';

    /**
     * Female
     */
    const FEMALE = 'F';

    /**
     * Cycling Player type
     */
    const TYPE_CP = 'CP';

    /**
     * Cycling Amator type
     */
    const TYPE_CA = 'CA';

}
