<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string $content
 * @property int $status
 * @property string $permission
 * @property int $user_id
 * @property int $image_id
 * @property int $menu_id
 * @property string $created_at
 * @property string $updated_at
 * @property-read \App\Models\Img $img
 * @property-read \App\Models\Menu $menu
 * @property-read \App\Models\User $user
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page active()
 */
class Page extends Model
{

    public $timestamps = true;
    protected $table = 'pages';
    protected $fillable = ['title', 'url', 'content', 'status', 'permission', 'user_id','lang',
        'image_id', 'menu_id'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    
    /**
     * Get the user record associated with the post.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the image record associated with the post.
     */
    public function img()
    {
        return $this->belongsTo('App\Models\Img', 'image_id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Models\Menu', 'page_id', 'id');
    }

    /**
     * Status No Active
     */
    const STATUS_NOACTIVE = 0;

    /**
     * Status Active
     */
    const STATUS_ACTIVE = 1;

}
