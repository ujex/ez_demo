<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Log
 *
 * @property int $id
 * @property string $symbol
 * @property int $foreign_id
 * @property string $message
 * @property string $path
 * @property string $post
 * @property string $user_agent
 * @property string $ip
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 * @mixin \Eloquent
 */
class Log extends Model
{

    public $timestamps = true;
    protected $table = 'logs';
    protected $fillable = ['symbol', 'foreign_id', 'message', 'path', 'post', 'user_agent',
        'ip', 'user_id'];

}
