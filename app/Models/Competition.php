<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Competition extends Model
{
    use SoftDeletes;

    public $timestamps = true;
    protected $table = 'competitions';
    protected $fillable = ['name', 'url', 'date_start', 'date_end', 'registration_to', 'season', 'place', 
        'code', 'lat', 'lng', 'info', 'register_form', 'registered_list',
        'geo', 'status', 'organiser', 
        'category_id',  'user_id'];
    protected $dates = ['date_start', 'date_end', 'registration_to', 'deleted_at'];

    public function getEndDateToJs()
    {
        $time = Carbon::parse($this->date_end);
        return $time->addDay()->format('Y-m-d');
    }

    public function getDateStartEnd()
    {
        $time_start = Carbon::parse($this->date_start);
        if ($this->date_end){
            $time_end = Carbon::parse($this->date_end);
            if ($time_end->month == $time_start->month){
                return $time_start->day . '-' . $time_end->day . $time_start->format('.m.Y');
            }else{
                return $time_start->day . $time_start->format('.m') . '-' . $time_end->day . $time_end->format('.m.Y');
            }
        }
        return $time_start->format('d.m.Y');
    }

    // Scopes
    public function scopeByCategory($query, $value)
    {
        return $query->where('category_id', $value);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeByDates($query, $date_start, $date_end)
    {
        return $query->where([
            ['date_start', '>=', $date_start],
            ['date_start', '<=', $date_end],
        ]);
    }

    /**
     * The ctg that belong to the competition.
     */
    public function ctgs()
    {
        return $this->belongsToMany('App\Models\Ctg');
    }

    /**
     * The files that belong to the competition.
     */
    public function files()
    {
        return $this->belongsToMany('App\Models\File');
    }

    /**
     * The file regulations that belong to the competition.
     */
    public function reg()
    {
        return $this->belongsToMany('App\Models\File')->where('category', File::CATEGORY_REGULATION);
    }

    /**
     * The file results that belong to the competition.
     */
    public function res()
    {
        return $this->belongsToMany('App\Models\File')->where('category', File::CATEGORY_RESULT);
    }

    /**
     * Get the category record associated with the competition.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function players()
    {
        return $this->hasMany('App\Models\Player');
    }

    /**
     * The rally that belong to the competition.
     */
    public function rally()
    {
        return $this->hasMany('App\Models\Rally')->where('status', 1)->orderBy('updated_at', 'desc');
    }
    
    public function save(array $options = [])
    {
        parent::save();
    }
    public function getBgColor()
    {
        return [
            1 => '#ff0000',
            2 => '#ea5d5d',
            3 => '#ffff00',
            4 => '#9cc3e6',
            5 => '#ccffcc',
            6 => '#efefef',
            0 => '#dedede',
        ][$this->category_id];
    }

    public function getTextColor()
    {
        return [
            0 => '#000000',
        ][0];
    }
}
