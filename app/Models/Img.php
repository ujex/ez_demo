<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Img
 *
 * @property int $id
 * @property string $name
 * @property string $name_original
 * @property string $author
 * @property string $description
 * @property string $type
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @mixin \Eloquent
 */
class Img extends Model
{

    public $timestamps = true;
    protected $table = 'images';
    protected $fillable = ['name', 'name_original', 'author', 'description', 'type', 'user_id'];

    /**
     * Get the posts for the image.
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
     * Type Thumbnail
     */
    const TYPE_THUMB = 'T';

    /**
     * Type Gallery
     */
    const TYPE_GALLERY = 'G';

}
