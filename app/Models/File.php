<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\File
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $name_original
 * @property string $description
 * @property string $category
 * @property string $mime_type
 * @property string $ext
 * @property string $source
 * @property int $status
 * @property int $downloads
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class File extends Model
{

    public $timestamps = true;
    protected $table = 'files';
    protected $fillable = ['name', 'path', 'name_original', 'description', 'category',
        'mime_type', 'ext', 'source', 'status', 'downloads', 'user_id'];
    protected $attributes = [
        'name' => '',
        'name_original' => '',
        'path' => '',
        'status' => 1,
    ];
    protected $dates = ['created_at', 'updated_at'];

    public function extIco()
    {
        switch ($this->ext){
            case 'doc':
            case 'docx':
            case 'odt':
                return 'fa-file-word-o';
            case 'pdf':
                return 'fa-file-pdf-o';
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
                return 'fa-file-photo-o';
            case 'xls':
            case 'xlsx':
                return 'fa-file-excel-o';
            default:
                return 'fa-file-o';
        }
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Get the user record associated with the file.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Allowed file extension
     */
    const ALLOWED_EXT = 'pdf,doc,docx,odt,xls,xlsx,jpg,jpeg,png,gif,gpx,xml';

    /**
     * Category Results
     */
    const CATEGORY_RESULT = 'result';

    /**
     * Category Regulation
     */
    const CATEGORY_REGULATION = 'regulation';

    /**
     * Category Others
     */
    const CATEGORY_OTHER = 'other';

}
