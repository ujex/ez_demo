<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    public $timestamps = true;
    protected $table = 'teams';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'name_clean', 'code', 'type', 'jersey',
        'zip', 'city', 'address', 'www', 'email', 'phone', 'ctg',
        'person_name', 'person_surname', 'person_phone', 'foregin_id',
        'status',
        'region_id', 'country_id'];
    protected $attributes = [
        'code' => null,
        'jersey' => null,
    ];

    public function scopeActive($query)
    {
        return $query->whereNotIn('ctg', [
            'Organizator',
        ])->where('status', 1);
    }
    
    /**
     * Get the country that owns the team.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the region that owns the team.
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }
    
    public function getAddress()
    {
        return $this->address . ', ' . $this->zip . ' ' . $this->city;
    }

    public function getWww()
    {
        if ($this->www && substr($this->www, 0, 4) != 'http'){
            $this->www = 'http://' . $this->www;
        }
        return $this->www;
    }

    public function save(array $options = [])
    {
        if ($this->name){
            $this->name_clean = str_slug($this->name);
        }
        parent::save();
    }

    /**
     * Amator Team
     */
    const TYPE_A = 'A';

    /**
     * Regional Team
     */
    const TYPE_R = 'R';

    /**
     * Continental Team
     */
    const TYPE_C = 'C';

    /**
     * Proffesional Continental Team
     */
    const TYPE_P = 'P';

    /**
     * World Tour Team
     */
    const TYPE_W = 'W';

    /**
     * Female Team
     */
    const TYPE_F = 'F';

}
