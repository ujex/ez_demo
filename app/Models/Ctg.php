<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ctg
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_short
 * @property string $name_range
 * @property string $sex
 * @property int $ord
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Competition[] $competitions
 * @mixin \Eloquent
 */
class Ctg extends Model
{

    public $timestamps = false;
    protected $table = 'ctg';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'name_en', 'name_short', 'range', 'sex', 'ord'];

    public static function getMap()
    {
        $category = self::where('ord', 1)
        ->orderBy('ord', 'asc')
        ->orderBy('range', 'asc')
        ->orderBy('sex', 'asc')
        ->get();
        $map = [];
        foreach ($category as $c){
            $map[$c->id] = $c->name_short;
        }
        return $map;
    }

    /**
     * The competitions that belong to the ctg.
     */
    public function competitions()
    {
        return $this->belongsToMany('App\Models\Competition');
    }

}
