<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Note
 *
 * @property int $id
 * @property string $code
 * @property string $content
 * @property string $type
 * @property string $title
 * @property int $status
 */
class Note extends Model
{

    public $timestamps = false;
    protected $table = 'notes';
    protected $fillable = ['code', 'content', 'type', 'title', 'status','lang'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Type string
     */
    const TYPE_STRING = 'string';

    /**
     * Type int
     */
    const TYPE_INT = 'int';

    /**
     * Type float
     */
    const TYPE_FLOAT = 'float';

    /**
     * Status No Active
     */
    const STATUS_NOACTIVE = 0;

    /**
     * Status Active
     */
    const STATUS_ACTIVE = 1;

}
