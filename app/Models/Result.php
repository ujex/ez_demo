<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{

    public $timestamps = false;
    protected $table = 'result';
    protected $fillable = ['lp', 'pos', 'bib', 'surname', 'name', 'uci_id', 'team', 'result', 'diff', 'info', 'rally_id',
        'country_id'];
    protected $dates = ['birth_date'];

}
