<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionCtgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competition_ctg', function(Blueprint $table)
		{
			$table->foreign('competition_id')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('ctg_id')->references('id')->on('ctg')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competition_ctg', function(Blueprint $table)
		{
			$table->dropForeign('competition_ctg_competition_id_foreign');
			$table->dropForeign('competition_ctg_ctg_id_foreign');
		});
	}

}
