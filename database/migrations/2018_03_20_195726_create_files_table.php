<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 256);
			$table->string('path', 64);
			$table->string('name_original', 128);
			$table->string('description', 256)->nullable();
			$table->string('category', 16)->default('result');
			$table->string('mime_type', 128)->nullable();
			$table->string('ext', 8)->nullable();
			$table->string('source', 256)->nullable();
			$table->boolean('status')->default(0);
			$table->integer('downloads')->unsigned()->default(0);
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('files');
	}

}
