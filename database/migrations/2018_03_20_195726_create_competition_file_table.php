<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_file', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('competition_id')->unsigned()->index('competition_file_competition_id_foreign');
			$table->integer('file_id')->unsigned()->index('competition_file_file_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_file');
	}

}
