<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionCtgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_ctg', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('competition_id')->unsigned()->index('competition_ctg_competition_id_foreign');
			$table->integer('ctg_id')->unsigned()->index('competition_ctg_ctg_id_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_ctg');
	}

}
