<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('surname');
			$table->string('name_clean');
			$table->date('birth_date');
			$table->enum('sex', array('M','F'));
			$table->string('uci_id', 16)->nullable();
			$table->string('uci_category', 64)->nullable();
			$table->string('type', 3)->default('CP');
			$table->string('license', 32)->nullable();
			$table->string('city')->nullable();
			$table->string('phone', 16)->nullable();
			$table->string('email', 64)->nullable();
			$table->date('medical')->nullable();
			$table->integer('rank_score')->default(0);
			$table->integer('rank_position')->default(9999);
			$table->text('address')->nullable();
			$table->integer('region_id')->unsigned()->nullable();
			$table->integer('country_id')->unsigned();
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('team_id')->unsigned()->nullable();
			$table->integer('competition_id')->unsigned()->nullable()->index('competition_id');
			$table->string('team', 256)->nullable();
			$table->integer('ez_id')->unsigned()->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
	}

}
