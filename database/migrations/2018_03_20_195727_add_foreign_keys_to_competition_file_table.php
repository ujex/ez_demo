<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competition_file', function(Blueprint $table)
		{
			$table->foreign('competition_id')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('file_id')->references('id')->on('files')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competition_file', function(Blueprint $table)
		{
			$table->dropForeign('competition_file_competition_id_foreign');
			$table->dropForeign('competition_file_file_id_foreign');
		});
	}

}
