<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teams', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('name_clean');
			$table->string('code', 3)->nullable();
			$table->string('type', 1)->default('R')->comment('A: amator, R: regional teams, C: continental, P: proffesional, W: world tour, F: female');
			$table->string('jersey', 32)->nullable();
			$table->boolean('status')->default(1);
			$table->string('person_phone', 32)->nullable();
			$table->string('person_surname', 64)->nullable();
			$table->string('person_name', 64)->nullable();
			$table->string('ctg', 64)->nullable();
			$table->string('phone', 32)->nullable();
			$table->string('email', 64)->nullable();
			$table->string('www', 128)->nullable();
			$table->string('address', 128)->nullable();
			$table->string('city', 128)->nullable();
			$table->string('zip', 16)->nullable();
			$table->integer('region_id')->unsigned()->nullable();
			$table->integer('country_id')->unsigned();
			$table->integer('user_id')->unsigned()->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teams');
	}

}
