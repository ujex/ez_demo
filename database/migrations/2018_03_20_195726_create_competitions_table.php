<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competitions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 256);
			$table->string('url', 256);
			$table->date('date_start');
			$table->date('date_end')->nullable();
			$table->date('registration_to')->nullable();
			$table->string('season', 10)->nullable();
			$table->string('place', 256)->nullable();
			$table->text('info', 65535)->nullable();
			$table->text('organiser')->nullable();
			$table->string('code', 128)->nullable();
			$table->string('lat', 32)->nullable();
			$table->string('lng', 32)->nullable();
			$table->text('geo', 65535)->nullable();
			$table->boolean('status')->nullable()->default(1);
			$table->integer('category_id')->unsigned()->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competitions');
	}

}
