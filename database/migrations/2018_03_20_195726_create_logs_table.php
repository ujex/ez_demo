<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('symbol', 32)->nullable();
			$table->integer('foreign_id')->unsigned()->nullable();
			$table->text('message', 65535)->nullable();
			$table->string('path', 256)->nullable();
			$table->text('post', 65535)->nullable();
			$table->string('user_agent', 256)->nullable();
			$table->string('ip', 32)->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logs');
	}

}
