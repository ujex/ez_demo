<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 256);
			$table->string('url', 256);
			$table->text('content', 65535);
			$table->boolean('status')->default(0);
			$table->string('permission', 32)->nullable();
			$table->string('lang', 2)->default('en');
			$table->integer('user_id')->unsigned();
			$table->integer('menu_id')->unsigned()->nullable();
			$table->integer('image_id')->unsigned()->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
