<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtgTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ctg', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 64);
			$table->string('name_en', 64)->nullable();
			$table->string('name_short', 16)->nullable();
			$table->string('range', 250);
			$table->enum('sex', array('M','F'));
			$table->boolean('ord')->default(127);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ctg');
	}

}
